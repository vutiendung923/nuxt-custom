import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _3a2faecc = () => interopDefault(import('../pages/artists/index.vue' /* webpackChunkName: "pages/artists/index" */))
const _0eae06d0 = () => interopDefault(import('../pages/dich-vu/index.vue' /* webpackChunkName: "pages/dich-vu/index" */))
const _305f1125 = () => interopDefault(import('../pages/inspire.vue' /* webpackChunkName: "pages/inspire" */))
const _3a6b9152 = () => interopDefault(import('../pages/lich-su-booking-khach-hang/index.vue' /* webpackChunkName: "pages/lich-su-booking-khach-hang/index" */))
const _fdd06058 = () => interopDefault(import('../pages/ve-chung-toi/index.vue' /* webpackChunkName: "pages/ve-chung-toi/index" */))
const _0756e7ca = () => interopDefault(import('../pages/lich-su-booking-khach-hang/comfirmBooking.vue' /* webpackChunkName: "pages/lich-su-booking-khach-hang/comfirmBooking" */))
const _8aeb2d82 = () => interopDefault(import('../pages/booking/_name.vue' /* webpackChunkName: "pages/booking/_name" */))
const _cbbaf81c = () => interopDefault(import('../pages/vab/_.vue' /* webpackChunkName: "pages/vab/_" */))
const _c78473b0 = () => interopDefault(import('../pages/news/_.vue' /* webpackChunkName: "pages/news/_" */))
const _206a9bdd = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/artists",
    component: _3a2faecc,
    name: "artists"
  }, {
    path: "/dich-vu",
    component: _0eae06d0,
    name: "dich-vu"
  }, {
    path: "/inspire",
    component: _305f1125,
    name: "inspire"
  }, {
    path: "/lich-su-booking-khach-hang",
    component: _3a6b9152,
    name: "lich-su-booking-khach-hang"
  }, {
    path: "/ve-chung-toi",
    component: _fdd06058,
    name: "ve-chung-toi"
  }, {
    path: "/lich-su-booking-khach-hang/comfirmBooking",
    component: _0756e7ca,
    name: "lich-su-booking-khach-hang-comfirmBooking"
  }, {
    path: "/booking/:name?",
    component: _8aeb2d82,
    name: "booking-name"
  }, {
    path: "/vab/*",
    component: _cbbaf81c,
    name: "vab-all"
  }, {
    path: "/news/*",
    component: _c78473b0,
    name: "news-all"
  }, {
    path: "/",
    component: _206a9bdd,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
