exports.ids = [21,20,22,25];
exports.modules = {

/***/ 294:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(295);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
__webpack_require__(8).default("5c8fbe94", content, true)

/***/ }),

/***/ 295:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(7);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".v-textarea textarea{align-self:stretch;flex:1 1 auto;line-height:1.75rem;max-width:100%;min-height:32px;outline:none;padding:0;width:100%}.v-textarea .v-text-field__prefix,.v-textarea .v-text-field__suffix{padding-top:2px;align-self:start}.v-textarea.v-text-field--box .v-text-field__prefix,.v-textarea.v-text-field--box textarea,.v-textarea.v-text-field--enclosed .v-text-field__prefix,.v-textarea.v-text-field--enclosed textarea{margin-top:24px}.v-textarea.v-text-field--box.v-text-field--outlined:not(.v-input--dense) .v-text-field__prefix,.v-textarea.v-text-field--box.v-text-field--outlined:not(.v-input--dense) .v-text-field__suffix,.v-textarea.v-text-field--box.v-text-field--outlined:not(.v-input--dense) textarea,.v-textarea.v-text-field--box.v-text-field--single-line:not(.v-input--dense) .v-text-field__prefix,.v-textarea.v-text-field--box.v-text-field--single-line:not(.v-input--dense) .v-text-field__suffix,.v-textarea.v-text-field--box.v-text-field--single-line:not(.v-input--dense) textarea,.v-textarea.v-text-field--enclosed.v-text-field--outlined:not(.v-input--dense) .v-text-field__prefix,.v-textarea.v-text-field--enclosed.v-text-field--outlined:not(.v-input--dense) .v-text-field__suffix,.v-textarea.v-text-field--enclosed.v-text-field--outlined:not(.v-input--dense) textarea,.v-textarea.v-text-field--enclosed.v-text-field--single-line:not(.v-input--dense) .v-text-field__prefix,.v-textarea.v-text-field--enclosed.v-text-field--single-line:not(.v-input--dense) .v-text-field__suffix,.v-textarea.v-text-field--enclosed.v-text-field--single-line:not(.v-input--dense) textarea{margin-top:10px}.v-textarea.v-text-field--box.v-text-field--outlined:not(.v-input--dense) .v-label,.v-textarea.v-text-field--box.v-text-field--single-line:not(.v-input--dense) .v-label,.v-textarea.v-text-field--enclosed.v-text-field--outlined:not(.v-input--dense) .v-label,.v-textarea.v-text-field--enclosed.v-text-field--single-line:not(.v-input--dense) .v-label{top:18px}.v-textarea.v-text-field--box.v-text-field--outlined.v-input--dense .v-text-field__prefix,.v-textarea.v-text-field--box.v-text-field--outlined.v-input--dense .v-text-field__suffix,.v-textarea.v-text-field--box.v-text-field--outlined.v-input--dense textarea,.v-textarea.v-text-field--box.v-text-field--single-line.v-input--dense .v-text-field__prefix,.v-textarea.v-text-field--box.v-text-field--single-line.v-input--dense .v-text-field__suffix,.v-textarea.v-text-field--box.v-text-field--single-line.v-input--dense textarea,.v-textarea.v-text-field--enclosed.v-text-field--outlined.v-input--dense .v-text-field__prefix,.v-textarea.v-text-field--enclosed.v-text-field--outlined.v-input--dense .v-text-field__suffix,.v-textarea.v-text-field--enclosed.v-text-field--outlined.v-input--dense textarea,.v-textarea.v-text-field--enclosed.v-text-field--single-line.v-input--dense .v-text-field__prefix,.v-textarea.v-text-field--enclosed.v-text-field--single-line.v-input--dense .v-text-field__suffix,.v-textarea.v-text-field--enclosed.v-text-field--single-line.v-input--dense textarea{margin-top:6px}.v-textarea.v-text-field--box.v-text-field--outlined.v-input--dense .v-input__append-inner,.v-textarea.v-text-field--box.v-text-field--outlined.v-input--dense .v-input__append-outer,.v-textarea.v-text-field--box.v-text-field--outlined.v-input--dense .v-input__prepend-inner,.v-textarea.v-text-field--box.v-text-field--outlined.v-input--dense .v-input__prepend-outer,.v-textarea.v-text-field--box.v-text-field--single-line.v-input--dense .v-input__append-inner,.v-textarea.v-text-field--box.v-text-field--single-line.v-input--dense .v-input__append-outer,.v-textarea.v-text-field--box.v-text-field--single-line.v-input--dense .v-input__prepend-inner,.v-textarea.v-text-field--box.v-text-field--single-line.v-input--dense .v-input__prepend-outer,.v-textarea.v-text-field--enclosed.v-text-field--outlined.v-input--dense .v-input__append-inner,.v-textarea.v-text-field--enclosed.v-text-field--outlined.v-input--dense .v-input__append-outer,.v-textarea.v-text-field--enclosed.v-text-field--outlined.v-input--dense .v-input__prepend-inner,.v-textarea.v-text-field--enclosed.v-text-field--outlined.v-input--dense .v-input__prepend-outer,.v-textarea.v-text-field--enclosed.v-text-field--single-line.v-input--dense .v-input__append-inner,.v-textarea.v-text-field--enclosed.v-text-field--single-line.v-input--dense .v-input__append-outer,.v-textarea.v-text-field--enclosed.v-text-field--single-line.v-input--dense .v-input__prepend-inner,.v-textarea.v-text-field--enclosed.v-text-field--single-line.v-input--dense .v-input__prepend-outer{align-self:flex-start;margin-top:8px}.v-textarea.v-text-field--solo{align-items:flex-start}.v-textarea.v-text-field--solo .v-input__append-inner,.v-textarea.v-text-field--solo .v-input__append-outer,.v-textarea.v-text-field--solo .v-input__prepend-inner,.v-textarea.v-text-field--solo .v-input__prepend-outer{align-self:flex-start;margin-top:12px}.v-application--is-ltr .v-textarea.v-text-field--solo .v-input__append-inner{padding-left:12px}.v-application--is-rtl .v-textarea.v-text-field--solo .v-input__append-inner{padding-right:12px}.v-textarea--auto-grow textarea{overflow:hidden}.v-textarea--no-resize textarea{resize:none}.v-textarea.v-text-field--enclosed .v-text-field__slot{align-self:stretch}.v-application--is-ltr .v-textarea.v-text-field--enclosed .v-text-field__slot{margin-right:-12px}.v-application--is-rtl .v-textarea.v-text-field--enclosed .v-text-field__slot{margin-left:-12px}.v-application--is-ltr .v-textarea.v-text-field--enclosed .v-text-field__slot textarea{padding-right:12px}.v-application--is-rtl .v-textarea.v-text-field--enclosed .v-text-field__slot textarea{padding-left:12px}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 300:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
const folders = {
  event: 'event'
};
/* unused harmony default export */ var _unused_webpack_default_export = (folders);

/***/ }),

/***/ 318:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _src_components_VTextarea_VTextarea_sass__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(294);
/* harmony import */ var _src_components_VTextarea_VTextarea_sass__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_src_components_VTextarea_VTextarea_sass__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _VTextField_VTextField__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(25);
/* harmony import */ var _util_mixins__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(2);
// Styles
 // Extensions

 // Utilities


const baseMixins = Object(_util_mixins__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(_VTextField_VTextField__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"]);
/* @vue/component */

/* harmony default export */ __webpack_exports__["a"] = (baseMixins.extend({
  name: 'v-textarea',
  props: {
    autoGrow: Boolean,
    noResize: Boolean,
    rowHeight: {
      type: [Number, String],
      default: 24,
      validator: v => !isNaN(parseFloat(v))
    },
    rows: {
      type: [Number, String],
      default: 5,
      validator: v => !isNaN(parseInt(v, 10))
    }
  },
  computed: {
    classes() {
      return {
        'v-textarea': true,
        'v-textarea--auto-grow': this.autoGrow,
        'v-textarea--no-resize': this.noResizeHandle,
        ..._VTextField_VTextField__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"].options.computed.classes.call(this)
      };
    },

    noResizeHandle() {
      return this.noResize || this.autoGrow;
    }

  },
  watch: {
    lazyValue() {
      this.autoGrow && this.$nextTick(this.calculateInputHeight);
    },

    rowHeight() {
      this.autoGrow && this.$nextTick(this.calculateInputHeight);
    }

  },

  mounted() {
    setTimeout(() => {
      this.autoGrow && this.calculateInputHeight();
    }, 0);
  },

  methods: {
    calculateInputHeight() {
      const input = this.$refs.input;
      if (!input) return;
      input.style.height = '0';
      const height = input.scrollHeight;
      const minHeight = parseInt(this.rows, 10) * parseFloat(this.rowHeight); // This has to be done ASAP, waiting for Vue
      // to update the DOM causes ugly layout jumping

      input.style.height = Math.max(minHeight, height) + 'px';
    },

    genInput() {
      const input = _VTextField_VTextField__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"].options.methods.genInput.call(this);
      input.tag = 'textarea';
      delete input.data.attrs.type;
      input.data.attrs.rows = this.rows;
      return input;
    },

    onInput(e) {
      _VTextField_VTextField__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"].options.methods.onInput.call(this, e);
      this.autoGrow && this.calculateInputHeight();
    },

    onKeyDown(e) {
      // Prevents closing of a
      // dialog when pressing
      // enter
      if (this.isFocused && e.keyCode === 13) {
        e.stopPropagation();
      }

      this.$emit('keydown', e);
    }

  }
}));

/***/ }),

/***/ 338:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaseItem; });
/* harmony import */ var _mixins_groupable__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(40);
/* harmony import */ var _util_mixins__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2);
/* harmony import */ var _util_console__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_3__);
// Mixins
 // Utilities


 // Types


/* @vue/component */

const BaseItem = vue__WEBPACK_IMPORTED_MODULE_3___default.a.extend({
  props: {
    activeClass: String,
    value: {
      required: false
    }
  },
  data: () => ({
    isActive: false
  }),
  methods: {
    toggle() {
      this.isActive = !this.isActive;
    }

  },

  render() {
    if (!this.$scopedSlots.default) {
      Object(_util_console__WEBPACK_IMPORTED_MODULE_2__[/* consoleWarn */ "c"])('v-item is missing a default scopedSlot', this);
      return null;
    }

    let element;
    /* istanbul ignore else */

    if (this.$scopedSlots.default) {
      element = this.$scopedSlots.default({
        active: this.isActive,
        toggle: this.toggle
      });
    }

    if (Array.isArray(element) && element.length === 1) {
      element = element[0];
    }

    if (!element || Array.isArray(element) || !element.tag) {
      Object(_util_console__WEBPACK_IMPORTED_MODULE_2__[/* consoleWarn */ "c"])('v-item should only contain a single element', this);
      return element;
    }

    element.data = this._b(element.data || {}, element.tag, {
      class: {
        [this.activeClass]: this.isActive
      }
    });
    return element;
  }

});
/* harmony default export */ __webpack_exports__["b"] = (Object(_util_mixins__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"])(BaseItem, Object(_mixins_groupable__WEBPACK_IMPORTED_MODULE_0__[/* factory */ "a"])('itemGroup', 'v-item', 'v-item-group')).extend({
  name: 'v-item'
}));

/***/ }),

/***/ 345:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/booking/tutorialBooking/index.vue?vue&type=template&id=d52dd946&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('v-dialog',{attrs:{"value":_vm.open,"max-width":"1000px","persistent":""},on:{"keydown":function($event){$event.key === 'Escape' && _vm.toggle()}}},[_c('v-card',{staticStyle:{"border-radius":"6px"}},[_c('v-img',{staticClass:"customImg",attrs:{"src":"/logo/anhNen.png"}},[_c('v-card-title',{staticClass:"pt-1 pb-7 pl-3 w-100",staticStyle:{"font-size":"1.12rem !important"}},[_vm._v("\n          Hướng dẫn\n        ")]),_vm._v(" "),_c('v-card-text',[_c('div',{staticClass:"text-center",staticStyle:{"position":"absolute","right":"0px","cursor":"pointer","top":"7px"},on:{"click":_vm.toggle}},[_c('v-img',{staticClass:"mr-2",attrs:{"height":"26","width":"26","src":"/logo/x.png"}})],1),_vm._v(" "),_c('div',{staticClass:"mb-12",domProps:{"innerHTML":_vm._s(_vm.dataDeatail)}})])],1)],1)],1)],1)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/booking/tutorialBooking/index.vue?vue&type=template&id=d52dd946&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/booking/tutorialBooking/index.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var tutorialBookingvue_type_script_lang_js_ = ({
  props: {
    open: {
      type: Boolean
    },
    path: {
      type: String,
      default: ''
    }
  },

  data() {
    return {
      dataDeatail: ''
    };
  },

  watch: {
    open(value) {
      this.getFooter();
    }

  },
  methods: {
    getFooter() {
      const data = {
        path: this.path
      };
      this.$store.dispatch('setting/detailFooter', data).then(res => {
        if (!res.error) {
          if ((res.data.data || []).length !== 0) {
            this.dataDeatail = res.data.data[0].content;
          }
        }
      });
    },

    toggle() {
      this.$emit('toggle');
    }

  }
});
// CONCATENATED MODULE: ./components/booking/tutorialBooking/index.vue?vue&type=script&lang=js&
 /* harmony default export */ var booking_tutorialBookingvue_type_script_lang_js_ = (tutorialBookingvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(14);

// EXTERNAL MODULE: ./node_modules/vuetify-loader/lib/runtime/installComponents.js
var installComponents = __webpack_require__(16);
var installComponents_default = /*#__PURE__*/__webpack_require__.n(installComponents);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/VCard.js
var VCard = __webpack_require__(100);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/index.js
var components_VCard = __webpack_require__(11);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VDialog/VDialog.js
var VDialog = __webpack_require__(284);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VImg/VImg.js + 2 modules
var VImg = __webpack_require__(78);

// CONCATENATED MODULE: ./components/booking/tutorialBooking/index.vue



function injectStyles (context) {
  
  
}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  booking_tutorialBookingvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "d52dd946",
  "ec280444"
  
)

/* harmony default export */ var tutorialBooking = __webpack_exports__["default"] = (component.exports);

/* vuetify-loader */






installComponents_default()(component, {VCard: VCard["a" /* default */],VCardText: components_VCard["b" /* VCardText */],VCardTitle: components_VCard["c" /* VCardTitle */],VDialog: VDialog["a" /* default */],VImg: VImg["a" /* default */]})


/***/ }),

/***/ 380:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(410);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(8).default
module.exports.__inject__ = function (context) {
  add("61c992eb", content, true, context)
};

/***/ }),

/***/ 381:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(412);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(8).default
module.exports.__inject__ = function (context) {
  add("5f958355", content, true, context)
};

/***/ }),

/***/ 409:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_addOther_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(380);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_addOther_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_addOther_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_addOther_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_addOther_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 410:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(7);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".customImg .v-responsive__sizer{padding-bottom:0!important}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 411:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_UpdateOther_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(381);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_UpdateOther_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_UpdateOther_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_UpdateOther_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_UpdateOther_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 412:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(7);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".customImg .v-responsive__sizer{padding-bottom:0!important}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 413:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(444);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(8).default
module.exports.__inject__ = function (context) {
  add("33d40fb4", content, true, context)
};

/***/ }),

/***/ 414:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(415);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
__webpack_require__(8).default("8f7a87bc", content, true)

/***/ }),

/***/ 415:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(7);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".v-slide-group{display:flex}.v-slide-group:not(.v-slide-group--has-affixes)>.v-slide-group__next,.v-slide-group:not(.v-slide-group--has-affixes)>.v-slide-group__prev{display:none}.v-slide-group.v-item-group>.v-slide-group__next,.v-slide-group.v-item-group>.v-slide-group__prev{cursor:pointer}.v-slide-item{display:inline-flex;flex:0 1 auto}.v-slide-group__next,.v-slide-group__prev{align-items:center;display:flex;flex:0 1 52px;justify-content:center;min-width:52px}.v-slide-group__content{display:flex;flex:1 0 auto;position:relative;transition:.3s cubic-bezier(.25,.8,.5,1);white-space:nowrap}.v-slide-group__wrapper{contain:content;display:flex;flex:1 1 auto;overflow:hidden}.v-slide-group__next--disabled,.v-slide-group__prev--disabled{pointer-events:none}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 423:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/booking/profile/addOther.vue?vue&type=template&id=1946e636&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('v-dialog',{attrs:{"value":_vm.open,"max-width":"700px","persistent":""},on:{"keydown":function($event){$event.key === 'Escape' && _vm.toggle()}}},[_c('v-card',[_c('v-img',{staticClass:"customImg",attrs:{"src":"/logo/anhNen.png"}},[_c('v-card-title',{staticClass:"pt-1 pb-1 w-100",staticStyle:{"font-size":"1.12rem !important"}},[_vm._v("\n          "+_vm._s(_vm.lang('$vuetify.pageBooking.addOtherProduct'))+"\n        ")]),_vm._v(" "),_c('v-card-text',{staticClass:"px-1"},[_c('v-container',{staticClass:"pa-0"},[_c('v-row',[_c('v-col',{staticClass:"px-1",attrs:{"cols":"6"}},[_c('v-img',{attrs:{"height":"19rem","width":"100%","src":_vm.$isNullOrEmpty(_vm.img.data) ? _vm.img.url : _vm.img.data},on:{"click":_vm.selectFileOpen}}),_vm._v(" "),(_vm.imgError)?_c('div',{staticClass:"pl-2",staticStyle:{"color":"#ff5252 !important","font-size":"12px"}},[_vm._v("\n                  "+_vm._s(_vm.lang('$vuetify.NotBlank'))+"\n                ")]):_vm._e(),_vm._v(" "),_c('v-file-input',{staticClass:"d-none",attrs:{"id":"input_file_other","accept":"image/png, image/jpeg, image/bmp","value":_vm.file},on:{"change":function($event){return _vm.inputFile($event)}}})],1),_vm._v(" "),_c('v-col',{attrs:{"cols":"6"}},[_c('v-text-field',{staticClass:"fs-15px fs-15",attrs:{"error-messages":_vm.nameSongErrors,"label":_vm.lang('$vuetify.pageBooking.addProduct.productName'),"dense":"","required":"","outlined":""},on:{"input":function($event){_vm.nameSongErrors = []}},model:{value:(_vm.nameSong),callback:function ($$v) {_vm.nameSong=$$v},expression:"nameSong"}}),_vm._v(" "),_c('v-text-field',{staticClass:"fs-15px fs-15",attrs:{"error-messages":_vm.singerErrors,"label":_vm.lang('$vuetify.pageBooking.hotProduct.codeProduct'),"dense":"","required":"","outlined":""},on:{"input":function($event){_vm.singerErrors = []}},model:{value:(_vm.singer),callback:function ($$v) {_vm.singer=$$v},expression:"singer"}}),_vm._v(" "),_c('v-text-field',{staticClass:"fs-15px fs-15",attrs:{"error-messages":_vm.priceErrors,"label":_vm.lang('$vuetify.pageBooking.hotProduct.priceProduct'),"dense":"","required":"","outlined":"","onKeyPress":"if(this.value.length==20) return false;"},on:{"keyup":_vm.checkValuePrice,"input":function($event){_vm.priceErrors = []}},model:{value:(_vm.price),callback:function ($$v) {_vm.price=$$v},expression:"price"}}),_vm._v(" "),_c('v-textarea',{staticClass:"fs-15px",attrs:{"error-messages":_vm.embeddedUrlErrors,"label":"Link embed","rows":"3","dense":"","required":"","outlined":""},on:{"input":function($event){_vm.embeddedUrlErrors = []}},model:{value:(_vm.embeddedUrl),callback:function ($$v) {_vm.embeddedUrl=$$v},expression:"embeddedUrl"}}),_vm._v(" "),_c('div',{staticClass:"d-flex justify-end pt-4"},[_c('v-btn',{staticClass:"custom-btn-normal mr-2",attrs:{"depressed":"","width":"110","color":"#1448C6"},on:{"click":_vm.checkValidate}},[_c('div',{staticClass:"font_size"},[_vm._v("\n                      "+_vm._s(_vm.$vuetify.lang.t('$vuetify.Update'))+"\n                    ")])]),_vm._v(" "),_c('v-btn',{staticClass:"custom-btn-normal",attrs:{"depressed":"","width":"110","color":"#EF9C0E"},on:{"click":_vm.toggle}},[_c('div',{staticClass:"font_size"},[_vm._v("\n                      "+_vm._s(_vm.$vuetify.lang.t('$vuetify.cancel'))+"\n                    ")])])],1)],1)],1)],1)],1)],1)],1)],1)],1)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/booking/profile/addOther.vue?vue&type=template&id=1946e636&

// EXTERNAL MODULE: ./assets/configurations/Folder_Config.js
var Folder_Config = __webpack_require__(300);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/booking/profile/addOther.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var addOthervue_type_script_lang_js_ = ({
  components: {},
  props: {
    open: {
      type: Boolean
    },
    data: {
      type: Object,
      default: null
    }
  },

  data() {
    return {
      imgError: false,
      nameSong: '',
      nameSongErrors: [],
      singer: '',
      singerErrors: [],
      embeddedUrl: '',
      price: null,
      priceErrors: [],
      embeddedUrlErrors: [],
      avatar: '',
      file: [],
      img: {
        url: '/logo/avatar.png',
        name: null,
        data: null
      }
    };
  },

  watch: {
    open(value) {
      this.img = {
        url: '/logo/avatar.png',
        name: null,
        data: null
      };
      this.nameSong = '';
      this.nameSongErrors = [];
      this.singer = '';
      this.singerErrors = [];
      this.price = null;
      this.priceErrors = [];
      this.embeddedUrl = '';
      this.embeddedUrlErrors = [];
      this.file = [];
      this.imgError = false;
    }

  },
  methods: {
    checkValuePrice() {
      this.price = this.$formatMoneyv2({
        amount: this.price
      });
    },

    lang(value) {
      if (value) {
        return this.$vuetify.lang.t(value);
      }
    },

    selectFileOpen() {
      return window.document.getElementById('input_file_other').click();
    },

    inputFile(value) {
      this.imgError = false;
      this.img = {
        url: '/logo/avatar.png',
        name: null,
        data: null
      };
      this.getBase64(value).then(data => {
        this.img = {
          url: null,
          name: value.name,
          data: data
        };
      });
    },

    getBase64(file) {
      return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);

        reader.onload = () => resolve(reader.result);

        reader.onerror = error => reject(error);
      });
    },

    update() {
      this.$store.dispatch('profile/addProduct', {
        code: this.singer,
        name: this.nameSong,
        embeddedUrl: this.embeddedUrl,
        amountMoney: Number(this.price.replace(/,/g, '')),
        imageUrl: null,
        file: {
          url: null,
          name: this.img.name,
          data: this.img.data
        }
      }).then(res => {
        if (!res.error) {
          this.$store.dispatch('notification/set_notifications', {
            type: 'success',
            color: 'success',
            text: this.lang('$vuetify.addOtherProductSuccess'),
            dark: true
          });
          this.toggle();
          this.$emit('success');
        }
      });
    },

    checkValidate() {
      let hasErrors = false;

      if (this.$isNullOrEmpty(this.nameSong)) {
        hasErrors = true;
        this.nameSongErrors = this.lang('$vuetify.NotBlank');
      }

      if (this.$isNullOrEmpty(this.price)) {
        hasErrors = true;
        this.priceErrors = ['Vui lòng nhập giá tiền'];
      }

      if (this.img.url === '/logo/avatar.png') {
        hasErrors = true;
        this.imgError = true;
      }

      if (this.$isNullOrEmpty(this.singer)) {
        hasErrors = true;
        this.singerErrors = this.lang('$vuetify.NotBlank');
      }

      if (this.$isNullOrEmpty(this.embeddedUrl)) {
        hasErrors = true;
        this.embeddedUrlErrors = this.lang('$vuetify.NotBlank');
      }

      if (!hasErrors) {
        this.update();
      }
    },

    toggle() {
      this.$emit('toggle');
    }

  }
});
// CONCATENATED MODULE: ./components/booking/profile/addOther.vue?vue&type=script&lang=js&
 /* harmony default export */ var profile_addOthervue_type_script_lang_js_ = (addOthervue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(14);

// EXTERNAL MODULE: ./node_modules/vuetify-loader/lib/runtime/installComponents.js
var installComponents = __webpack_require__(16);
var installComponents_default = /*#__PURE__*/__webpack_require__.n(installComponents);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VBtn/VBtn.js
var VBtn = __webpack_require__(54);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/VCard.js
var VCard = __webpack_require__(100);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/index.js
var components_VCard = __webpack_require__(11);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VCol.js
var VCol = __webpack_require__(281);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VContainer.js + 1 modules
var VContainer = __webpack_require__(290);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VDialog/VDialog.js
var VDialog = __webpack_require__(284);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VFileInput/VFileInput.js
var VFileInput = __webpack_require__(286);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VImg/VImg.js + 2 modules
var VImg = __webpack_require__(78);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VRow.js
var VRow = __webpack_require__(283);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VTextField/VTextField.js + 3 modules
var VTextField = __webpack_require__(25);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VTextarea/VTextarea.js
var VTextarea = __webpack_require__(318);

// CONCATENATED MODULE: ./components/booking/profile/addOther.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(409)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  profile_addOthervue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "d77bab76"
  
)

/* harmony default export */ var addOther = __webpack_exports__["default"] = (component.exports);

/* vuetify-loader */













installComponents_default()(component, {VBtn: VBtn["a" /* default */],VCard: VCard["a" /* default */],VCardText: components_VCard["b" /* VCardText */],VCardTitle: components_VCard["c" /* VCardTitle */],VCol: VCol["a" /* default */],VContainer: VContainer["a" /* default */],VDialog: VDialog["a" /* default */],VFileInput: VFileInput["a" /* default */],VImg: VImg["a" /* default */],VRow: VRow["a" /* default */],VTextField: VTextField["a" /* default */],VTextarea: VTextarea["a" /* default */]})


/***/ }),

/***/ 424:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/booking/profile/UpdateOther.vue?vue&type=template&id=5ef0ef16&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('v-dialog',{attrs:{"value":_vm.open,"max-width":"700px","persistent":""},on:{"keydown":function($event){$event.key === 'Escape' && _vm.toggle()}}},[_c('v-card',[_c('v-img',{staticClass:"customImg",attrs:{"src":"/logo/anhNen.png"}},[_c('v-card-title',{staticClass:"pt-1 pb-1 w-100",staticStyle:{"font-size":"1.12rem !important"}},[_vm._v("\n          "+_vm._s(_vm.lang('$vuetify.pageBooking.UpdateOtherProduct'))+"\n        ")]),_vm._v(" "),_c('v-card-text',{staticClass:"px-1"},[_c('v-container',{staticClass:"pa-0"},[_c('v-row',[_c('v-col',{staticClass:"px-1",attrs:{"cols":"6"}},[_c('v-img',{attrs:{"height":"19rem","width":"100%","src":_vm.$isNullOrEmpty(_vm.img.data) ? _vm.img.url : _vm.img.data},on:{"click":_vm.selectFileOpen}}),_vm._v(" "),_c('v-file-input',{staticClass:"d-none",attrs:{"id":"input_file_updateOther","accept":"image/png, image/jpeg, image/bmp","value":_vm.file},on:{"change":function($event){return _vm.inputFile($event)}}})],1),_vm._v(" "),_c('v-col',{attrs:{"cols":"6"}},[_c('v-text-field',{staticClass:"fs-15px fs-15",attrs:{"error-messages":_vm.nameSongErrors,"label":_vm.lang('$vuetify.pageBooking.addProduct.productName'),"dense":"","required":"","outlined":""},on:{"input":function($event){_vm.nameSongErrors = []}},model:{value:(_vm.nameSong),callback:function ($$v) {_vm.nameSong=$$v},expression:"nameSong"}}),_vm._v(" "),_c('v-text-field',{staticClass:"fs-15px fs-15",attrs:{"error-messages":_vm.singerErrors,"label":_vm.lang('$vuetify.pageBooking.hotProduct.codeProduct'),"dense":"","required":"","outlined":""},on:{"input":function($event){_vm.singerErrors = []}},model:{value:(_vm.singer),callback:function ($$v) {_vm.singer=$$v},expression:"singer"}}),_vm._v(" "),_c('v-text-field',{staticClass:"fs-15px fs-15",attrs:{"error-messages":_vm.priceErrors,"label":_vm.lang('$vuetify.pageBooking.hotProduct.priceProduct'),"dense":"","required":"","outlined":""},on:{"keyup":_vm.checkValuePrice,"input":function($event){_vm.priceErrors = []}},model:{value:(_vm.price),callback:function ($$v) {_vm.price=$$v},expression:"price"}}),_vm._v(" "),_c('v-textarea',{staticClass:"fs-15px",attrs:{"error-messages":_vm.embeddedUrlErrors,"label":"Link embed","rows":"3","dense":"","required":"","outlined":""},on:{"input":function($event){_vm.embeddedUrlErrors = []}},model:{value:(_vm.embeddedUrl),callback:function ($$v) {_vm.embeddedUrl=$$v},expression:"embeddedUrl"}}),_vm._v(" "),_c('div',{staticClass:"d-flex justify-end pt-4"},[_c('v-btn',{staticClass:"custom-btn-normal mr-2",attrs:{"depressed":"","width":"110","color":"#1448C6"},on:{"click":_vm.checkValidate}},[_c('div',{staticClass:"font_size"},[_vm._v("\n                      "+_vm._s(_vm.$vuetify.lang.t('$vuetify.Update'))+"\n                    ")])]),_vm._v(" "),_c('v-btn',{staticClass:"custom-btn-normal",attrs:{"depressed":"","width":"110","color":"#EF9C0E"},on:{"click":_vm.toggle}},[_c('div',{staticClass:"font_size"},[_vm._v("\n                      "+_vm._s(_vm.$vuetify.lang.t('$vuetify.cancel'))+"\n                    ")])])],1)],1)],1)],1)],1)],1)],1)],1)],1)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/booking/profile/UpdateOther.vue?vue&type=template&id=5ef0ef16&

// EXTERNAL MODULE: ./assets/configurations/Base_Url.js
var Base_Url = __webpack_require__(63);

// EXTERNAL MODULE: ./assets/configurations/Folder_Config.js
var Folder_Config = __webpack_require__(300);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/booking/profile/UpdateOther.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ var UpdateOthervue_type_script_lang_js_ = ({
  components: {},
  props: {
    open: {
      type: Boolean
    },
    data: {
      type: Object,
      default: null
    }
  },

  data() {
    return {
      nameSong: '',
      nameSongErrors: [],
      singer: '',
      singerErrors: [],
      embeddedUrl: '',
      embeddedUrlErrors: [],
      price: '',
      priceErrors: [],
      avatar: '',
      file: [],
      img: {
        url: '/logo/avatar.png',
        data: null,
        name: null
      }
    };
  },

  watch: {
    open(value) {
      this.img = {
        url: Base_Url["a" /* default */].urlImg + this.data.imageUrl,
        data: null,
        name: null
      };
      this.nameSong = this.data.name;
      this.nameSongErrors = [];
      this.singer = this.data.code;
      this.price = this.$formatMoneyv2({
        amount: this.data.amountMoney
      });
      this.priceErrors = [];
      this.singerErrors = [];
      this.embeddedUrl = this.data.embeddedUrl;
      this.embeddedUrlErrors = [];
      this.file = [];
      this.avatar = '';
    }

  },
  methods: {
    checkValuePrice() {
      this.price = this.$formatMoneyv2({
        amount: this.price
      });
    },

    lang(value) {
      if (value) {
        return this.$vuetify.lang.t(value);
      }
    },

    selectFileOpen() {
      return window.document.getElementById('input_file_updateOther').click();
    },

    inputFile(value) {
      this.imgError = false;
      this.img = {
        url: '/logo/avatar.png',
        name: null,
        data: null
      };
      this.getBase64(value).then(data => {
        this.img = {
          url: null,
          name: value.name,
          data: data
        };
      });
    },

    getBase64(file) {
      return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);

        reader.onload = () => resolve(reader.result);

        reader.onerror = error => reject(error);
      });
    },

    update() {
      this.$store.dispatch('profile/updateProduct', {
        id: this.data.id,
        code: this.singer,
        name: this.nameSong,
        embeddedUrl: this.embeddedUrl,
        amountMoney: Number(this.price.replace(/,/g, '')),
        imageUrl: null,
        file: {
          url: this.$isNullOrEmpty(this.img.url) ? null : this.img.url.replace('https://cms.vab.xteldev.com/file/upload/', ''),
          data: this.img.data,
          name: this.img.name
        }
      }).then(res => {
        if (!res.error) {
          this.$store.dispatch('notification/set_notifications', {
            type: 'success',
            color: 'success',
            text: this.lang('$vuetify.updateOtherProductSuccess'),
            dark: true
          });
          this.toggle();
          this.$emit('success');
        }
      });
    },

    checkValidate() {
      let hasErrors = false;

      if (this.$isNullOrEmpty(this.nameSong)) {
        hasErrors = true;
        this.nameSongErrors = ['Vui lòng nhập đây đủ thông tin'];
      }

      if (this.$isNullOrEmpty(this.singer)) {
        hasErrors = true;
        this.singerErrors = ['Vui lòng nhập đây đủ thông tin'];
      }

      if (this.$isNullOrEmpty(this.price)) {
        hasErrors = true;
        this.priceErrors = ['Vui lòng nhập giá tiền'];
      }

      if (this.$isNullOrEmpty(this.embeddedUrl)) {
        hasErrors = true;
        this.embeddedUrlErrors = ['Vui lòng nhập đây đủ thông tin'];
      }

      if (!hasErrors) {
        this.update();
      }
    },

    toggle() {
      this.$emit('toggle');
    }

  }
});
// CONCATENATED MODULE: ./components/booking/profile/UpdateOther.vue?vue&type=script&lang=js&
 /* harmony default export */ var profile_UpdateOthervue_type_script_lang_js_ = (UpdateOthervue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(14);

// EXTERNAL MODULE: ./node_modules/vuetify-loader/lib/runtime/installComponents.js
var installComponents = __webpack_require__(16);
var installComponents_default = /*#__PURE__*/__webpack_require__.n(installComponents);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VBtn/VBtn.js
var VBtn = __webpack_require__(54);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/VCard.js
var VCard = __webpack_require__(100);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/index.js
var components_VCard = __webpack_require__(11);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VCol.js
var VCol = __webpack_require__(281);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VContainer.js + 1 modules
var VContainer = __webpack_require__(290);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VDialog/VDialog.js
var VDialog = __webpack_require__(284);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VFileInput/VFileInput.js
var VFileInput = __webpack_require__(286);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VImg/VImg.js + 2 modules
var VImg = __webpack_require__(78);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VRow.js
var VRow = __webpack_require__(283);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VTextField/VTextField.js + 3 modules
var VTextField = __webpack_require__(25);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VTextarea/VTextarea.js
var VTextarea = __webpack_require__(318);

// CONCATENATED MODULE: ./components/booking/profile/UpdateOther.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(411)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  profile_UpdateOthervue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "038dd47e"
  
)

/* harmony default export */ var UpdateOther = __webpack_exports__["default"] = (component.exports);

/* vuetify-loader */













installComponents_default()(component, {VBtn: VBtn["a" /* default */],VCard: VCard["a" /* default */],VCardText: components_VCard["b" /* VCardText */],VCardTitle: components_VCard["c" /* VCardTitle */],VCol: VCol["a" /* default */],VContainer: VContainer["a" /* default */],VDialog: VDialog["a" /* default */],VFileInput: VFileInput["a" /* default */],VImg: VImg["a" /* default */],VRow: VRow["a" /* default */],VTextField: VTextField["a" /* default */],VTextarea: VTextarea["a" /* default */]})


/***/ }),

/***/ 443:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_productOther_vue_vue_type_style_index_0_id_f2fcee78_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(413);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_productOther_vue_vue_type_style_index_0_id_f2fcee78_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_productOther_vue_vue_type_style_index_0_id_f2fcee78_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_productOther_vue_vue_type_style_index_0_id_f2fcee78_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_productOther_vue_vue_type_style_index_0_id_f2fcee78_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 444:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(7);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".ImgProductOther[data-v-f2fcee78]{display:flex!important;justify-content:center;align-items:center}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 445:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaseSlideGroup; });
/* harmony import */ var _src_components_VSlideGroup_VSlideGroup_sass__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(414);
/* harmony import */ var _src_components_VSlideGroup_VSlideGroup_sass__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_src_components_VSlideGroup_VSlideGroup_sass__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _VIcon__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(13);
/* harmony import */ var _transitions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(29);
/* harmony import */ var _VItemGroup_VItemGroup__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(64);
/* harmony import */ var _mixins_mobile__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(82);
/* harmony import */ var _directives_resize__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(41);
/* harmony import */ var _directives_touch__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(66);
/* harmony import */ var _util_mixins__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(2);
// Styles
 // Components


 // Extensions

 // Mixins

 // Directives


 // Utilities


const BaseSlideGroup = Object(_util_mixins__WEBPACK_IMPORTED_MODULE_7__[/* default */ "a"])(_VItemGroup_VItemGroup__WEBPACK_IMPORTED_MODULE_3__[/* BaseItemGroup */ "a"], _mixins_mobile__WEBPACK_IMPORTED_MODULE_4__[/* default */ "a"]).extend({
  name: 'base-slide-group',
  directives: {
    Resize: _directives_resize__WEBPACK_IMPORTED_MODULE_5__[/* default */ "a"],
    Touch: _directives_touch__WEBPACK_IMPORTED_MODULE_6__[/* default */ "a"]
  },
  props: {
    activeClass: {
      type: String,
      default: 'v-slide-item--active'
    },
    centerActive: Boolean,
    nextIcon: {
      type: String,
      default: '$next'
    },
    prevIcon: {
      type: String,
      default: '$prev'
    },
    showArrows: {
      type: [Boolean, String],
      validator: v => typeof v === 'boolean' || ['always', 'desktop', 'mobile'].includes(v)
    }
  },
  data: () => ({
    internalItemsLength: 0,
    isOverflowing: false,
    resizeTimeout: 0,
    startX: 0,
    isSwipingHorizontal: false,
    isSwiping: false,
    scrollOffset: 0,
    widths: {
      content: 0,
      wrapper: 0
    }
  }),
  computed: {
    canTouch() {
      return typeof window !== 'undefined';
    },

    __cachedNext() {
      return this.genTransition('next');
    },

    __cachedPrev() {
      return this.genTransition('prev');
    },

    classes() {
      return { ..._VItemGroup_VItemGroup__WEBPACK_IMPORTED_MODULE_3__[/* BaseItemGroup */ "a"].options.computed.classes.call(this),
        'v-slide-group': true,
        'v-slide-group--has-affixes': this.hasAffixes,
        'v-slide-group--is-overflowing': this.isOverflowing
      };
    },

    hasAffixes() {
      switch (this.showArrows) {
        // Always show arrows on desktop & mobile
        case 'always':
          return true;
        // Always show arrows on desktop

        case 'desktop':
          return !this.isMobile;
        // Show arrows on mobile when overflowing.
        // This matches the default 2.2 behavior

        case true:
          return this.isOverflowing || Math.abs(this.scrollOffset) > 0;
        // Always show on mobile

        case 'mobile':
          return this.isMobile || this.isOverflowing || Math.abs(this.scrollOffset) > 0;
        // https://material.io/components/tabs#scrollable-tabs
        // Always show arrows when
        // overflowed on desktop

        default:
          return !this.isMobile && (this.isOverflowing || Math.abs(this.scrollOffset) > 0);
      }
    },

    hasNext() {
      if (!this.hasAffixes) return false;
      const {
        content,
        wrapper
      } = this.widths; // Check one scroll ahead to know the width of right-most item

      return content > Math.abs(this.scrollOffset) + wrapper;
    },

    hasPrev() {
      return this.hasAffixes && this.scrollOffset !== 0;
    }

  },
  watch: {
    internalValue: 'setWidths',
    // When overflow changes, the arrows alter
    // the widths of the content and wrapper
    // and need to be recalculated
    isOverflowing: 'setWidths',

    scrollOffset(val) {
      this.$refs.content.style.transform = `translateX(${-val}px)`;
    }

  },

  beforeUpdate() {
    this.internalItemsLength = (this.$children || []).length;
  },

  updated() {
    if (this.internalItemsLength === (this.$children || []).length) return;
    this.setWidths();
  },

  methods: {
    // Always generate next for scrollable hint
    genNext() {
      const slot = this.$scopedSlots.next ? this.$scopedSlots.next({}) : this.$slots.next || this.__cachedNext;
      return this.$createElement('div', {
        staticClass: 'v-slide-group__next',
        class: {
          'v-slide-group__next--disabled': !this.hasNext
        },
        on: {
          click: () => this.onAffixClick('next')
        },
        key: 'next'
      }, [slot]);
    },

    genContent() {
      return this.$createElement('div', {
        staticClass: 'v-slide-group__content',
        ref: 'content'
      }, this.$slots.default);
    },

    genData() {
      return {
        class: this.classes,
        directives: [{
          name: 'resize',
          value: this.onResize
        }]
      };
    },

    genIcon(location) {
      let icon = location;

      if (this.$vuetify.rtl && location === 'prev') {
        icon = 'next';
      } else if (this.$vuetify.rtl && location === 'next') {
        icon = 'prev';
      }

      const upperLocation = `${location[0].toUpperCase()}${location.slice(1)}`;
      const hasAffix = this[`has${upperLocation}`];
      if (!this.showArrows && !hasAffix) return null;
      return this.$createElement(_VIcon__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"], {
        props: {
          disabled: !hasAffix
        }
      }, this[`${icon}Icon`]);
    },

    // Always generate prev for scrollable hint
    genPrev() {
      const slot = this.$scopedSlots.prev ? this.$scopedSlots.prev({}) : this.$slots.prev || this.__cachedPrev;
      return this.$createElement('div', {
        staticClass: 'v-slide-group__prev',
        class: {
          'v-slide-group__prev--disabled': !this.hasPrev
        },
        on: {
          click: () => this.onAffixClick('prev')
        },
        key: 'prev'
      }, [slot]);
    },

    genTransition(location) {
      return this.$createElement(_transitions__WEBPACK_IMPORTED_MODULE_2__[/* VFadeTransition */ "d"], [this.genIcon(location)]);
    },

    genWrapper() {
      return this.$createElement('div', {
        staticClass: 'v-slide-group__wrapper',
        directives: [{
          name: 'touch',
          value: {
            start: e => this.overflowCheck(e, this.onTouchStart),
            move: e => this.overflowCheck(e, this.onTouchMove),
            end: e => this.overflowCheck(e, this.onTouchEnd)
          }
        }],
        ref: 'wrapper'
      }, [this.genContent()]);
    },

    calculateNewOffset(direction, widths, rtl, currentScrollOffset) {
      const sign = rtl ? -1 : 1;
      const newAbosluteOffset = sign * currentScrollOffset + (direction === 'prev' ? -1 : 1) * widths.wrapper;
      return sign * Math.max(Math.min(newAbosluteOffset, widths.content - widths.wrapper), 0);
    },

    onAffixClick(location) {
      this.$emit(`click:${location}`);
      this.scrollTo(location);
    },

    onResize() {
      /* istanbul ignore next */
      if (this._isDestroyed) return;
      this.setWidths();
    },

    onTouchStart(e) {
      const {
        content
      } = this.$refs;
      this.startX = this.scrollOffset + e.touchstartX;
      content.style.setProperty('transition', 'none');
      content.style.setProperty('willChange', 'transform');
    },

    onTouchMove(e) {
      if (!this.canTouch) return;

      if (!this.isSwiping) {
        // only calculate disableSwipeHorizontal during the first onTouchMove invoke
        // in order to ensure disableSwipeHorizontal value is consistent between onTouchStart and onTouchEnd
        const diffX = e.touchmoveX - e.touchstartX;
        const diffY = e.touchmoveY - e.touchstartY;
        this.isSwipingHorizontal = Math.abs(diffX) > Math.abs(diffY);
        this.isSwiping = true;
      }

      if (this.isSwipingHorizontal) {
        // sliding horizontally
        this.scrollOffset = this.startX - e.touchmoveX; // temporarily disable window vertical scrolling

        document.documentElement.style.overflowY = 'hidden';
      }
    },

    onTouchEnd() {
      if (!this.canTouch) return;
      const {
        content,
        wrapper
      } = this.$refs;
      const maxScrollOffset = content.clientWidth - wrapper.clientWidth;
      content.style.setProperty('transition', null);
      content.style.setProperty('willChange', null);

      if (this.$vuetify.rtl) {
        /* istanbul ignore else */
        if (this.scrollOffset > 0 || !this.isOverflowing) {
          this.scrollOffset = 0;
        } else if (this.scrollOffset <= -maxScrollOffset) {
          this.scrollOffset = -maxScrollOffset;
        }
      } else {
        /* istanbul ignore else */
        if (this.scrollOffset < 0 || !this.isOverflowing) {
          this.scrollOffset = 0;
        } else if (this.scrollOffset >= maxScrollOffset) {
          this.scrollOffset = maxScrollOffset;
        }
      }

      this.isSwiping = false; // rollback whole page scrolling to default

      document.documentElement.style.removeProperty('overflow-y');
    },

    overflowCheck(e, fn) {
      e.stopPropagation();
      this.isOverflowing && fn(e);
    },

    scrollIntoView
    /* istanbul ignore next */
    () {
      if (!this.selectedItem && this.items.length) {
        const lastItemPosition = this.items[this.items.length - 1].$el.getBoundingClientRect();
        const wrapperPosition = this.$refs.wrapper.getBoundingClientRect();

        if (this.$vuetify.rtl && wrapperPosition.right < lastItemPosition.right || !this.$vuetify.rtl && wrapperPosition.left > lastItemPosition.left) {
          this.scrollTo('prev');
        }
      }

      if (!this.selectedItem) {
        return;
      }

      if (this.selectedIndex === 0 || !this.centerActive && !this.isOverflowing) {
        this.scrollOffset = 0;
      } else if (this.centerActive) {
        this.scrollOffset = this.calculateCenteredOffset(this.selectedItem.$el, this.widths, this.$vuetify.rtl);
      } else if (this.isOverflowing) {
        this.scrollOffset = this.calculateUpdatedOffset(this.selectedItem.$el, this.widths, this.$vuetify.rtl, this.scrollOffset);
      }
    },

    calculateUpdatedOffset(selectedElement, widths, rtl, currentScrollOffset) {
      const clientWidth = selectedElement.clientWidth;
      const offsetLeft = rtl ? widths.content - selectedElement.offsetLeft - clientWidth : selectedElement.offsetLeft;

      if (rtl) {
        currentScrollOffset = -currentScrollOffset;
      }

      const totalWidth = widths.wrapper + currentScrollOffset;
      const itemOffset = clientWidth + offsetLeft;
      const additionalOffset = clientWidth * 0.4;

      if (offsetLeft <= currentScrollOffset) {
        currentScrollOffset = Math.max(offsetLeft - additionalOffset, 0);
      } else if (totalWidth <= itemOffset) {
        currentScrollOffset = Math.min(currentScrollOffset - (totalWidth - itemOffset - additionalOffset), widths.content - widths.wrapper);
      }

      return rtl ? -currentScrollOffset : currentScrollOffset;
    },

    calculateCenteredOffset(selectedElement, widths, rtl) {
      const {
        offsetLeft,
        clientWidth
      } = selectedElement;

      if (rtl) {
        const offsetCentered = widths.content - offsetLeft - clientWidth / 2 - widths.wrapper / 2;
        return -Math.min(widths.content - widths.wrapper, Math.max(0, offsetCentered));
      } else {
        const offsetCentered = offsetLeft + clientWidth / 2 - widths.wrapper / 2;
        return Math.min(widths.content - widths.wrapper, Math.max(0, offsetCentered));
      }
    },

    scrollTo
    /* istanbul ignore next */
    (location) {
      this.scrollOffset = this.calculateNewOffset(location, {
        // Force reflow
        content: this.$refs.content ? this.$refs.content.clientWidth : 0,
        wrapper: this.$refs.wrapper ? this.$refs.wrapper.clientWidth : 0
      }, this.$vuetify.rtl, this.scrollOffset);
    },

    setWidths
    /* istanbul ignore next */
    () {
      window.requestAnimationFrame(() => {
        const {
          content,
          wrapper
        } = this.$refs;
        this.widths = {
          content: content ? content.clientWidth : 0,
          wrapper: wrapper ? wrapper.clientWidth : 0
        }; // https://github.com/vuetifyjs/vuetify/issues/13212
        // We add +1 to the wrappers width to prevent an issue where the `clientWidth`
        // gets calculated wrongly by the browser if using a different zoom-level.

        this.isOverflowing = this.widths.wrapper + 1 < this.widths.content;
        this.scrollIntoView();
      });
    }

  },

  render(h) {
    return h('div', this.genData(), [this.genPrev(), this.genWrapper(), this.genNext()]);
  }

});
/* harmony default export */ __webpack_exports__["b"] = (BaseSlideGroup.extend({
  name: 'v-slide-group',

  provide() {
    return {
      slideGroup: this
    };
  }

}));

/***/ }),

/***/ 462:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _VItemGroup_VItem__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(338);
/* harmony import */ var _mixins_groupable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(40);
/* harmony import */ var _util_mixins__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(2);
// Extensions
 // Mixins



/* harmony default export */ __webpack_exports__["a"] = (Object(_util_mixins__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(_VItemGroup_VItem__WEBPACK_IMPORTED_MODULE_0__[/* BaseItem */ "a"], Object(_mixins_groupable__WEBPACK_IMPORTED_MODULE_1__[/* factory */ "a"])('slideGroup')
/* @vue/component */
).extend({
  name: 'v-slide-item'
}));

/***/ }),

/***/ 475:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/booking/profile/productOther.vue?vue&type=template&id=f2fcee78&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('v-row',[_c('v-col',{attrs:{"cols":"1"}}),_vm._v(" "),_c('v-col',{staticClass:"text-center py-0 px-1",attrs:{"cols":"10"}},[_c('h2',{staticStyle:{"font-size":"28px"}},[_vm._v("\n        "+_vm._s(_vm.$vuetify.lang.t('$vuetify.pageBooking.otherProducts'))+"\n      ")])]),_vm._v(" "),_c('v-col',{staticClass:"py-0 pr-0",attrs:{"cols":"1"}},[(
          _vm.$store.state.login.role
            ? _vm.$store.state.login.role.includes('ARTIST')
            : ''
        )?_c('div',{staticClass:"d-flex",staticStyle:{"justify-content":"flex-end"}},[_c('v-menu',{attrs:{"left":"","transition":"scroll-y-transition","offset-y":"","offset-x":"","nudge-bottom":"5","nudge-right":"52"},scopedSlots:_vm._u([{key:"activator",fn:function(ref){
        var on = ref.on;
return [_c('div',_vm._g({},on),[_c('div',_vm._g({staticClass:"box-menu"},on),[_c('v-icon',{attrs:{"large":"","color":"#00000073"}},[_vm._v("\n                  "+_vm._s('mdi-dots-horizontal')+"\n                ")])],1)])]}}],null,false,3702150472)},[_vm._v(" "),_c('v-list',{staticClass:"pa-2 border-list",staticStyle:{"background-color":"#22318b !important"},attrs:{"dense":"","width":"12.4375rem"}},[_c('v-list-item-group',{model:{value:(_vm.actionProduct),callback:function ($$v) {_vm.actionProduct=$$v},expression:"actionProduct"}},[_c('v-list-item',{staticClass:"pl-0",on:{"click":function($event){_vm.openAdd = true}}},[_c('v-list-item-icon',{staticClass:"mr-2"},[_c('v-img',{attrs:{"height":"20","width":"20","src":"/logo/add.png"}})],1),_vm._v(" "),_c('v-list-item-content',[_c('v-list-item-title',{staticClass:"white--text"},[_c('div',{staticStyle:{"font-weight":"400"}},[_vm._v("\n                      "+_vm._s(_vm.$vuetify.lang.t('$vuetify.addProduct'))+"\n                    ")])])],1)],1),_vm._v(" "),_c('v-divider',{attrs:{"color":"#525478"}}),_vm._v(" "),_c('v-list-item',{staticClass:"pl-0"},[_c('v-list-item-icon',{staticClass:"mr-2"},[_c('v-img',{attrs:{"height":"20","width":"20","src":"/logo/detail.png"}})],1),_vm._v(" "),_c('v-list-item-content',[_c('v-list-item-title',{staticClass:"white--text"},[_c('div',{staticStyle:{"font-weight":"400"}},[_vm._v("\n                      "+_vm._s(_vm.$vuetify.lang.t('$vuetify.updateProduct'))+"\n                    ")])])],1)],1),_vm._v(" "),_c('v-divider',{attrs:{"color":"#525478"}}),_vm._v(" "),_c('v-list-item',{staticClass:"pl-0"},[_c('v-list-item-icon',{staticClass:"mr-2"},[_c('v-img',{attrs:{"height":"20","width":"20","src":"/logo/deleteIcon.png"}})],1),_vm._v(" "),_c('v-list-item-content',[_c('v-list-item-title',{staticClass:"nuxt-link white--text"},[_c('div',{staticStyle:{"font-weight":"400"}},[_vm._v("\n                      "+_vm._s(_vm.$vuetify.lang.t('$vuetify.deleteProduct'))+"\n                    ")])])],1)],1),_vm._v(" "),_c('v-divider',{attrs:{"color":"#525478"}}),_vm._v(" "),_c('v-list-item',{staticClass:"pl-0",on:{"click":function($event){_vm.openTutorialOther = true}}},[_c('v-list-item-icon',{staticClass:"mr-2"},[_c('v-img',{attrs:{"height":"20","width":"20","src":"/logo/hoicham.png"}})],1),_vm._v(" "),_c('v-list-item-content',[_c('v-list-item-title',{staticClass:"nuxt-link white--text"},[_c('div',{staticStyle:{"font-weight":"400"}},[_vm._v("\n                      "+_vm._s(_vm.$vuetify.lang.t('$vuetify.Tutorial'))+"\n                    ")])])],1)],1)],1)],1)],1)],1):_vm._e()]),_vm._v(" "),((_vm.listnghesi || []).length !== 0)?_c('v-col',{staticClass:"px-0 mb-12 pt-5",attrs:{"cols":"12"}},[_c('v-sheet',{staticClass:"mx-auto",attrs:{"elevation":"0","max-width":"100%"}},[_c('v-slide-group',{attrs:{"center-active":"","show-arrows":""},model:{value:(_vm.model),callback:function ($$v) {_vm.model=$$v},expression:"model"}},_vm._l((_vm.listnghesi),function(item){return _c('v-slide-item',{key:item.id},[(!_vm.$isNullOrEmpty(_vm.actionProduct))?_c('a',{staticClass:"black--text",staticStyle:{"text-decoration":"none"},attrs:{"flat":"","target":"_blank"}},[_c('v-img',{staticClass:"ma-4 mb-3 mr-10",staticStyle:{"border-radius":"89px"},attrs:{"src":_vm.UrlImg(item.imageUrl),"width":_vm.$store.state.app.viewPort == 'mobile' ? 70 : 110,"height":_vm.$store.state.app.viewPort == 'mobile' ? 70 : 110}},[_c('div',{staticClass:"h-100 ImgProductOther"},[(_vm.actionProduct === 1)?_c('div',[_c('v-img',{attrs:{"src":"/logo/detail.png","height":"29","width":"29"},on:{"click":function($event){return _vm.openDetail(item)}}})],1):_vm._e(),_vm._v(" "),(_vm.actionProduct === 2)?_c('div',{on:{"click":function($event){return _vm.deleteSp(item)}}},[_c('v-img',{attrs:{"height":"29","width":"29","src":"/logo/deleteIcon.png"}})],1):_vm._e()])]),_vm._v(" "),_c('div',{staticClass:"text-center fs-15 mr-10"},[_vm._v("\n                "+_vm._s(item.name)+"\n              ")])],1):_c('a',{staticClass:"black--text",staticStyle:{"text-decoration":"none"},attrs:{"flat":"","href":!_vm.$isNullOrEmpty(_vm.actionProduct) ? '' : item.embeddedUrl,"target":"_blank"}},[_c('v-img',{staticClass:"ma-4 mb-3 mr-10",staticStyle:{"border-radius":"89px"},attrs:{"src":_vm.UrlImg(item.imageUrl),"width":_vm.$store.state.app.viewPort == 'mobile' ? 70 : 110,"height":_vm.$store.state.app.viewPort == 'mobile' ? 70 : 110}},[_c('div',{staticClass:"h-100 ImgProductOther"},[(_vm.actionProduct === 1)?_c('div',[_c('v-img',{attrs:{"src":"/logo/detail.png","height":"29","width":"29"},on:{"click":function($event){return _vm.openDetail(item)}}})],1):_vm._e(),_vm._v(" "),(_vm.actionProduct === 2)?_c('div',{on:{"click":function($event){return _vm.deleteSp(item)}}},[_c('v-img',{attrs:{"height":"29","width":"29","src":"/logo/deleteIcon.png"}})],1):_vm._e()])]),_vm._v(" "),_c('div',{staticClass:"text-center fs-15 mr-10 text-truncate",staticStyle:{"max-width":"150px"}},[_vm._v("\n                "+_vm._s(item.name)+"\n              ")])],1)])}),1)],1)],1):_vm._e()],1),_vm._ssrNode(" "),_c('yes-no-alert',{attrs:{"alertmsg":_vm.$vuetify.lang.t('$vuetify.sureDelete'),"open":_vm.opendelete},on:{"toggle":function($event){_vm.opendelete = !_vm.opendelete},"OK":_vm.deletePRD}}),_vm._ssrNode(" "),_c('updateProduct',{attrs:{"open":_vm.updateProduct,"data":_vm.dataDetail ? _vm.dataDetail : {}},on:{"toggle":function($event){_vm.updateProduct = !_vm.updateProduct},"success":function($event){return _vm.success()}}}),_vm._ssrNode(" "),_c('addProduct',{attrs:{"open":_vm.openAdd},on:{"toggle":function($event){_vm.openAdd = !_vm.openAdd},"success":function($event){return _vm.success()}}}),_vm._ssrNode(" "),_c('tutorialBooking',{attrs:{"open":_vm.openTutorialOther,"path":"/huong-dan-san-pham-khac"},on:{"toggle":function($event){_vm.openTutorialOther = !_vm.openTutorialOther}}})],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/booking/profile/productOther.vue?vue&type=template&id=f2fcee78&scoped=true&

// EXTERNAL MODULE: ./components/YesNoAlert.vue + 4 modules
var YesNoAlert = __webpack_require__(81);

// EXTERNAL MODULE: ./assets/configurations/Base_Url.js
var Base_Url = __webpack_require__(63);

// EXTERNAL MODULE: ./components/booking/profile/addOther.vue + 4 modules
var addOther = __webpack_require__(423);

// EXTERNAL MODULE: ./components/booking/profile/UpdateOther.vue + 4 modules
var UpdateOther = __webpack_require__(424);

// EXTERNAL MODULE: ./components/booking/tutorialBooking/index.vue + 4 modules
var tutorialBooking = __webpack_require__(345);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/booking/profile/productOther.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ var productOthervue_type_script_lang_js_ = ({
  components: {
    YesNoAlert: YesNoAlert["default"],
    updateProduct: UpdateOther["default"],
    addProduct: addOther["default"],
    tutorialBooking: tutorialBooking["default"]
  },
  props: {
    listnghesi: {
      type: Array,
      default: null
    }
  },

  data() {
    return {
      openTutorialOther: false,
      dataDetail: '',
      opendelete: false,
      updateProduct: false,
      actionProduct: '',
      openAdd: false,
      model: null,
      idSp: ''
    };
  },

  methods: {
    success() {
      this.actionProduct = '';
      this.$emit('success');
    },

    UrlImg(value) {
      return Base_Url["a" /* default */].urlImg + value;
    },

    deletePRD() {
      this.$store.dispatch('profile/deleteProduct', {
        productId: this.idSp
      }).then(res => {
        if (!res.error) {
          this.$store.dispatch('notification/set_notifications', {
            type: 'success',
            color: 'success',
            text: 'Xóa sản phẩm nổi bật thành công',
            dark: true
          });
          this.$emit('success');
          this.actionProduct = '';
        }
      });
    },

    openDetail(value) {
      this.dataDetail = value;
      this.updateProduct = !this.updateProduct;
    },

    deleteSp(value) {
      this.opendelete = true;
      this.idSp = value.id;
    }

  }
});
// CONCATENATED MODULE: ./components/booking/profile/productOther.vue?vue&type=script&lang=js&
 /* harmony default export */ var profile_productOthervue_type_script_lang_js_ = (productOthervue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(14);

// EXTERNAL MODULE: ./node_modules/vuetify-loader/lib/runtime/installComponents.js
var installComponents = __webpack_require__(16);
var installComponents_default = /*#__PURE__*/__webpack_require__.n(installComponents);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VCol.js
var VCol = __webpack_require__(281);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VDivider/VDivider.js
var VDivider = __webpack_require__(103);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VIcon/VIcon.js
var VIcon = __webpack_require__(72);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VImg/VImg.js + 2 modules
var VImg = __webpack_require__(78);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VList/VList.js
var VList = __webpack_require__(104);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VList/VListItem.js
var VListItem = __webpack_require__(55);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VList/index.js + 2 modules
var components_VList = __webpack_require__(23);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VList/VListItemGroup.js
var VListItemGroup = __webpack_require__(106);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VList/VListItemIcon.js
var VListItemIcon = __webpack_require__(47);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VMenu/VMenu.js
var VMenu = __webpack_require__(107);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VRow.js
var VRow = __webpack_require__(283);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VSheet/VSheet.js
var VSheet = __webpack_require__(24);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VSlideGroup/VSlideGroup.js
var VSlideGroup = __webpack_require__(445);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VSlideGroup/VSlideItem.js
var VSlideItem = __webpack_require__(462);

// CONCATENATED MODULE: ./components/booking/profile/productOther.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(443)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  profile_productOthervue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "f2fcee78",
  "1f370292"
  
)

/* harmony default export */ var productOther = __webpack_exports__["default"] = (component.exports);

/* nuxt-component-imports */
installComponents_default()(component, {YesNoAlert: __webpack_require__(81).default})


/* vuetify-loader */
















installComponents_default()(component, {VCol: VCol["a" /* default */],VDivider: VDivider["a" /* default */],VIcon: VIcon["a" /* default */],VImg: VImg["a" /* default */],VList: VList["a" /* default */],VListItem: VListItem["a" /* default */],VListItemContent: components_VList["a" /* VListItemContent */],VListItemGroup: VListItemGroup["a" /* default */],VListItemIcon: VListItemIcon["a" /* default */],VListItemTitle: components_VList["c" /* VListItemTitle */],VMenu: VMenu["a" /* default */],VRow: VRow["a" /* default */],VSheet: VSheet["a" /* default */],VSlideGroup: VSlideGroup["b" /* default */],VSlideItem: VSlideItem["a" /* default */]})


/***/ })

};;
//# sourceMappingURL=booking-profile-product-other.js.map