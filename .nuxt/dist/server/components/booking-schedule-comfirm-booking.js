exports.ids = [24];
exports.modules = {

/***/ 426:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/booking/schedule/comfirmBooking.vue?vue&type=template&id=8ba36b5c&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('v-dialog',{attrs:{"value":_vm.open,"max-width":"700","persistent":""},on:{"keydown":function($event){$event.key === 'Escape' && _vm.toggle()}}},[_c('v-card',{staticStyle:{"border-radius":"15px"}},[_c('v-img',{staticClass:"customImg",attrs:{"src":"/logo/anhNen.png"}},[_c('v-card-text',{staticClass:"mb-4 px-1"},[(
              _vm.$store.state.login.role &&
              _vm.$store.state.login.role.includes('CUSTOMER')
            )?_c('div',{staticClass:"text-center",staticStyle:{"position":"absolute","right":"0px","cursor":"pointer","top":"7px"},on:{"click":_vm.toggle}},[_c('v-img',{staticClass:"mr-2",attrs:{"height":"26","width":"26","src":"/logo/x.png"}})],1):_vm._e(),_vm._v(" "),_c('div',[_c('v-row',[_c('v-col',{staticClass:"pt-0",attrs:{"cols":"12","md":"12"}},[_c('v-card-title',{staticClass:"d-flex justify-center align-center black--text pt-0 pb-2 font-weight-regular",staticStyle:{"color":"#1648be !important","border-bottom":"1px dashed","border-color":"#1648be6b","font-size":"25px"}},[_vm._v("\n                  "+_vm._s('Lịch diễn')+"\n                ")]),_vm._v(" "),(_vm.data)?_c('v-card-text',{staticClass:"pa-0"},[_c('v-row',[_c('v-col',{staticClass:"px-0 border-text",attrs:{"cols":"12"}},[_c('div',{staticClass:"d-flex align-center"},[_c('v-img',{staticClass:"mr-4",attrs:{"height":"28","max-width":"22","width":"22","src":"/VAB_booking/location.png"}}),_vm._v(" "),_c('div',{staticClass:"black--text fs-16"},[_vm._v("\n                          "+_vm._s(_vm.data.address)+"\n                        ")])],1)]),_vm._v(" "),_c('v-col',{staticClass:"px-0 border-text",attrs:{"cols":"12"}},[_c('div',{staticClass:"d-flex align-center"},[_c('v-img',{staticClass:"mr-4",attrs:{"height":"22","max-width":"22","width":"22","src":"/VAB_booking/event.png"}}),_vm._v(" "),_c('div',{staticClass:"fs-16",staticStyle:{"color":"#088d08"}},[_c('span',[_vm._v(_vm._s(_vm.data.content))])])],1)]),_vm._v(" "),_c('v-col',{staticClass:"px-0 border-text",attrs:{"cols":"12"}},[_c('v-row',[_c('v-col',{staticClass:"color-text fs-16 pb-0 px-0",attrs:{"cols":"5","md":"3"}},[_vm._v("\n                          "+_vm._s('Mã Booking:')+"\n                        ")]),_vm._v(" "),_c('v-col',{staticClass:"pb-0 black--text",attrs:{"cols":"7","md":"9"}},[_c('span',[_vm._v(_vm._s(_vm.data.bookingCode))])]),_vm._v(" "),(!_vm.$isNullOrEmpty(_vm.data.artistName))?[_c('v-col',{staticClass:"color-text fs-16 pb-0 px-0",attrs:{"cols":"5","md":"3"}},[_vm._v("\n                            "+_vm._s('Tên nghệ sĩ:')+"\n                          ")]),_vm._v(" "),_c('v-col',{staticClass:"pb-0 black--text",attrs:{"cols":"7","md":"9"}},[_c('span',[_vm._v(_vm._s(_vm.data.artistName))])])]:_vm._e(),_vm._v(" "),(!_vm.$isNullOrEmpty(_vm.data.fromTime))?[_c('v-col',{staticClass:"color-text fs-16 pb-0 px-0",attrs:{"cols":"5","md":"3"}},[_vm._v("\n                            "+_vm._s('Thời gian bắt đầu:')+"\n                          ")]),_vm._v(" "),_c('v-col',{staticClass:"pb-0 black--text",attrs:{"cols":"7","md":"9"}},[_vm._v("\n                            "+_vm._s(_vm.data.fromTime.slice(0, 10))+"\n                          ")])]:_vm._e(),_vm._v(" "),(!_vm.$isNullOrEmpty(_vm.data.toTime))?[_c('v-col',{staticClass:"color-text fs-16 pb-0 px-0",attrs:{"cols":"5","md":"3"}},[_vm._v("\n                            "+_vm._s('Thời gian kết thúc:')+"\n                          ")]),_vm._v(" "),_c('v-col',{staticClass:"pb-0 black--text",attrs:{"cols":"7","md":"9"}},[_vm._v("\n                            "+_vm._s(_vm.data.toTime.slice(0, 10))+"\n                          ")])]:_vm._e(),_vm._v(" "),_c('v-col',{staticClass:"color-text fs-16 pb-0 px-0",attrs:{"cols":"5","md":"3"}},[_vm._v("\n                          "+_vm._s('Ngân sách dự kiến:')+"\n                        ")]),_vm._v(" "),_c('v-col',{staticClass:"pb-0 black--text",attrs:{"cols":"7","md":"9"}},[_vm._v("\n                          "+_vm._s(_vm.$formatMoney({ amount: _vm.data.expectFee }))+" VNĐ\n                        ")]),_vm._v(" "),_c('v-col',{staticClass:"color-text fs-16 pb-0 px-0",attrs:{"cols":"5","md":"3"}},[_vm._v("\n                          "+_vm._s('Số điện thoại:')+"\n                        ")]),_vm._v(" "),_c('v-col',{staticClass:"pb-0 black--text",attrs:{"cols":"7","md":"9"}},[(
                              _vm.$store.state.login.role &&
                              _vm.$store.state.login.role.includes('ARTIST')
                            )?[_vm._v("\n                            "+_vm._s(_vm.data.phone)+"\n                          ")]:[_vm._v("\n                            "+_vm._s(_vm.data.mnPhone)+"\n                          ")]],2),_vm._v(" "),(!_vm.$isNullOrEmpty(_vm.data.description))?[_c('v-col',{staticClass:"color-text fs-16 pb-0 px-0",attrs:{"cols":"5","md":"3"}},[_vm._v("\n                            "+_vm._s('Ghi chú thêm :')+"\n                          ")]),_vm._v(" "),_c('v-col',{staticClass:"pb-0 black--text",attrs:{"cols":"7","md":"9"}},[_vm._v("\n                            "+_vm._s(_vm.data.description)+"\n                          ")])]:_vm._e()],2)],1),_vm._v(" "),_c('v-col',{staticClass:"px-0",attrs:{"cols":"12"}},[_c('div',{staticClass:"d-flex align-center"},[_c('v-img',{staticClass:"mr-3",attrs:{"height":"22","max-width":"22","src":"/VAB_booking/newStatus.png"}}),_vm._v(" "),_c('span',{staticClass:"black--text fs-16 font-weight-bold pb-0"},[_vm._v("\n                          "+_vm._s('Trạng thái lịch diễn:')+"\n                        ")])],1)]),_vm._v(" "),_c('v-col',{staticClass:"pa-0",attrs:{"cols":"4"}},[_c('v-checkbox',{staticClass:"black-text fs-14",attrs:{"label":"Chờ xác nhận","value":"0","hide-details":""},model:{value:(_vm.selected),callback:function ($$v) {_vm.selected=$$v},expression:"selected"}})],1),_vm._v(" "),_c('v-col',{staticClass:"pa-0",attrs:{"cols":"4"}},[_c('v-checkbox',{staticClass:"black-text fs-14",attrs:{"label":"Đã thực hiện","value":"3","hide-details":""},model:{value:(_vm.selected),callback:function ($$v) {_vm.selected=$$v},expression:"selected"}})],1),_vm._v(" "),_c('v-col',{staticClass:"pa-0",attrs:{"cols":"4"}},[_c('v-checkbox',{staticClass:"black-text fs-14",attrs:{"label":"Chưa thực hiện","value":"1","hide-details":""},model:{value:(_vm.selected),callback:function ($$v) {_vm.selected=$$v},expression:"selected"}})],1),_vm._v(" "),_c('v-col',{staticClass:"pa-0",attrs:{"cols":"4"}},[_c('v-checkbox',{staticClass:"black-text fs-14",attrs:{"label":"Hoãn lịch","value":"4","hide-details":""},model:{value:(_vm.selected),callback:function ($$v) {_vm.selected=$$v},expression:"selected"}})],1),_vm._v(" "),_c('v-col',{staticClass:"pa-0",attrs:{"cols":"4"}},[_c('v-checkbox',{staticClass:"black-text fs-14",attrs:{"label":"Hủy lịch","value":"2","hide-details":""},model:{value:(_vm.selected),callback:function ($$v) {_vm.selected=$$v},expression:"selected"}})],1),_vm._v(" "),_c('v-col',{staticClass:"pa-0",attrs:{"cols":"4"}},[_c('v-checkbox',{staticClass:"black-text fs-14",attrs:{"label":"Thay đổi lịch","value":"5","hide-details":""},model:{value:(_vm.selected),callback:function ($$v) {_vm.selected=$$v},expression:"selected"}})],1),_vm._v(" "),(_vm.checkUser())?_c('v-col',{staticClass:"text-center pb-0 pt-12",attrs:{"cols":"12"}},[_c('v-btn',{staticClass:"custom-btn-normal mr-2",attrs:{"depressed":"","width":"35%","color":"#1448C6"},on:{"click":function($event){return _vm.acceptBooking(_vm.selected)}}},[_c('div',{staticClass:"font_size"},[_vm._v("\n                          "+_vm._s('Xác nhận')+"\n                        ")])]),_vm._v(" "),_c('v-btn',{staticClass:"custom-btn-normal",attrs:{"depressed":"","width":"35%","color":"#EF9C0E"},on:{"click":_vm.toggle}},[_c('div',{staticClass:"font_size"},[_vm._v("\n                          "+_vm._s(_vm.$vuetify.lang.t('$vuetify.cancel'))+"\n                        ")])])],1):_vm._e()],1)],1):_vm._e()],1)],1)],1)])],1)],1)],1),_vm._ssrNode(" "),_c('yes-no-alert',{attrs:{"alertmsg":"Bạn có chắc muốn từ chối booking?","open":_vm.openDialogYesNo},on:{"toggle":function($event){_vm.openDialogYesNo = !_vm.openDialogYesNo},"OK":function($event){return _vm.acceptBooking(2)}}})],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/booking/schedule/comfirmBooking.vue?vue&type=template&id=8ba36b5c&

// EXTERNAL MODULE: ./components/YesNoAlert.vue + 4 modules
var YesNoAlert = __webpack_require__(81);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/booking/schedule/comfirmBooking.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var comfirmBookingvue_type_script_lang_js_ = ({
  components: {
    YesNoAlert: YesNoAlert["default"]
  },
  props: {
    open: {
      type: Boolean
    },
    data: {
      type: Object,
      default: null
    },
    id: {
      type: Number,
      default: null
    }
  },

  data() {
    return {
      selected: '',
      nameCustomer: '',
      gmail: '',
      phoneNumber: '',
      facebook: '',
      openDialogYesNo: false
    };
  },

  watch: {
    selected(value) {
      console.log(value);
    },

    open() {
      if (this.data.performStatus) {
        this.selected = String(this.data.performStatus);
      }
    }

  },
  methods: {
    checkUser() {
      return this.$store.state.login.role && !this.$store.state.login.role.includes('CUSTOMER');
    },

    async acceptBooking(status) {
      const res = await this.$store.dispatch('booking/updateLichDien', {
        id: this.data.id,
        artistId: this.id,
        performStatus: Number(this.selected)
      });

      if (!res.error) {
        this.toggle();
        this.$emit('success');
        this.$store.dispatch('notification/set_notifications', {
          type: 'success',
          color: 'success',
          text: 'Cập nhật trạng thái lịch diễn thành công',
          dark: true
        });
      }
    },

    toggle() {
      this.$emit('toggle');
    }

  }
});
// CONCATENATED MODULE: ./components/booking/schedule/comfirmBooking.vue?vue&type=script&lang=js&
 /* harmony default export */ var schedule_comfirmBookingvue_type_script_lang_js_ = (comfirmBookingvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(14);

// EXTERNAL MODULE: ./node_modules/vuetify-loader/lib/runtime/installComponents.js
var installComponents = __webpack_require__(16);
var installComponents_default = /*#__PURE__*/__webpack_require__.n(installComponents);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VBtn/VBtn.js
var VBtn = __webpack_require__(54);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/VCard.js
var VCard = __webpack_require__(100);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/index.js
var components_VCard = __webpack_require__(11);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCheckbox/VCheckbox.js + 1 modules
var VCheckbox = __webpack_require__(291);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VCol.js
var VCol = __webpack_require__(281);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VDialog/VDialog.js
var VDialog = __webpack_require__(284);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VImg/VImg.js + 2 modules
var VImg = __webpack_require__(78);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VRow.js
var VRow = __webpack_require__(283);

// CONCATENATED MODULE: ./components/booking/schedule/comfirmBooking.vue



function injectStyles (context) {
  
  
}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  schedule_comfirmBookingvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "23835792"
  
)

/* harmony default export */ var comfirmBooking = __webpack_exports__["default"] = (component.exports);

/* nuxt-component-imports */
installComponents_default()(component, {YesNoAlert: __webpack_require__(81).default})


/* vuetify-loader */










installComponents_default()(component, {VBtn: VBtn["a" /* default */],VCard: VCard["a" /* default */],VCardText: components_VCard["b" /* VCardText */],VCardTitle: components_VCard["c" /* VCardTitle */],VCheckbox: VCheckbox["a" /* default */],VCol: VCol["a" /* default */],VDialog: VDialog["a" /* default */],VImg: VImg["a" /* default */],VRow: VRow["a" /* default */]})


/***/ })

};;
//# sourceMappingURL=booking-schedule-comfirm-booking.js.map