exports.ids = [25];
exports.modules = {

/***/ 345:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/booking/tutorialBooking/index.vue?vue&type=template&id=d52dd946&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('v-dialog',{attrs:{"value":_vm.open,"max-width":"1000px","persistent":""},on:{"keydown":function($event){$event.key === 'Escape' && _vm.toggle()}}},[_c('v-card',{staticStyle:{"border-radius":"6px"}},[_c('v-img',{staticClass:"customImg",attrs:{"src":"/logo/anhNen.png"}},[_c('v-card-title',{staticClass:"pt-1 pb-7 pl-3 w-100",staticStyle:{"font-size":"1.12rem !important"}},[_vm._v("\n          Hướng dẫn\n        ")]),_vm._v(" "),_c('v-card-text',[_c('div',{staticClass:"text-center",staticStyle:{"position":"absolute","right":"0px","cursor":"pointer","top":"7px"},on:{"click":_vm.toggle}},[_c('v-img',{staticClass:"mr-2",attrs:{"height":"26","width":"26","src":"/logo/x.png"}})],1),_vm._v(" "),_c('div',{staticClass:"mb-12",domProps:{"innerHTML":_vm._s(_vm.dataDeatail)}})])],1)],1)],1)],1)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/booking/tutorialBooking/index.vue?vue&type=template&id=d52dd946&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/booking/tutorialBooking/index.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var tutorialBookingvue_type_script_lang_js_ = ({
  props: {
    open: {
      type: Boolean
    },
    path: {
      type: String,
      default: ''
    }
  },

  data() {
    return {
      dataDeatail: ''
    };
  },

  watch: {
    open(value) {
      this.getFooter();
    }

  },
  methods: {
    getFooter() {
      const data = {
        path: this.path
      };
      this.$store.dispatch('setting/detailFooter', data).then(res => {
        if (!res.error) {
          if ((res.data.data || []).length !== 0) {
            this.dataDeatail = res.data.data[0].content;
          }
        }
      });
    },

    toggle() {
      this.$emit('toggle');
    }

  }
});
// CONCATENATED MODULE: ./components/booking/tutorialBooking/index.vue?vue&type=script&lang=js&
 /* harmony default export */ var booking_tutorialBookingvue_type_script_lang_js_ = (tutorialBookingvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(14);

// EXTERNAL MODULE: ./node_modules/vuetify-loader/lib/runtime/installComponents.js
var installComponents = __webpack_require__(16);
var installComponents_default = /*#__PURE__*/__webpack_require__.n(installComponents);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/VCard.js
var VCard = __webpack_require__(100);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/index.js
var components_VCard = __webpack_require__(11);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VDialog/VDialog.js
var VDialog = __webpack_require__(284);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VImg/VImg.js + 2 modules
var VImg = __webpack_require__(78);

// CONCATENATED MODULE: ./components/booking/tutorialBooking/index.vue



function injectStyles (context) {
  
  
}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  booking_tutorialBookingvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "d52dd946",
  "ec280444"
  
)

/* harmony default export */ var tutorialBooking = __webpack_exports__["default"] = (component.exports);

/* vuetify-loader */






installComponents_default()(component, {VCard: VCard["a" /* default */],VCardText: components_VCard["b" /* VCardText */],VCardTitle: components_VCard["c" /* VCardTitle */],VDialog: VDialog["a" /* default */],VImg: VImg["a" /* default */]})


/***/ })

};;
//# sourceMappingURL=booking-tutorial-booking.js.map