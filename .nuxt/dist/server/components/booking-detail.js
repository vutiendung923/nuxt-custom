exports.ids = [11];
exports.modules = {

/***/ 317:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(368);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(8).default
module.exports.__inject__ = function (context) {
  add("e1191802", content, true, context)
};

/***/ }),

/***/ 367:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_detail_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(317);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_detail_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_detail_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_detail_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_detail_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 368:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(7);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".color-text{color:#1f51c5}.border-text{border-bottom:1px solid;border-color:hsla(0,0%,43.9%,.38824)!important}.hoverText input{cursor:pointer!important}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 383:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/booking/detail.vue?vue&type=template&id=6aa13412&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('v-dialog',{attrs:{"value":_vm.open,"max-width":"750px","persistent":""},on:{"keydown":function($event){$event.key === 'Escape' && _vm.toggle()}}},[_c('v-card',{staticStyle:{"border-radius":"15px"}},[_c('v-img',{staticClass:"customImg",attrs:{"src":"/logo/anhNen.png"}},[_c('v-card-text',[_c('div',{staticClass:"text-center",staticStyle:{"position":"absolute","right":"0px","cursor":"pointer","top":"7px"},on:{"click":_vm.toggle}},[_c('v-img',{staticClass:"mr-2",attrs:{"height":"26","width":"26","src":"/logo/x.png"}})],1),_vm._v(" "),_c('div',[(!_vm.$isNullOrEmpty(_vm.data))?_c('v-row',[_c('v-col',{staticClass:"pt-0 mb-7",attrs:{"cols":"12","md":"12"}},[_c('v-card-text',{staticClass:"pa-0"},[_c('v-row',[_c('v-col',{staticClass:"px-0 pb-2 border-text",staticStyle:{"text-align":"-webkit-center"},attrs:{"cols":"12"}},[_c('v-img',{staticClass:"mr-4",attrs:{"height":"60px","width":"60px","src":"/icon/icon_v.png"}}),_vm._v(" "),_c('div',{staticClass:"fw-600 fs-18 pt-2 pb-1"},[_vm._v("\n                        Bạn đã booking thành công\n                      ")]),_vm._v(" "),_c('div',{staticClass:"primary--text fs-15 font-italic"},[_vm._v("\n                        Chúng tôi sẽ kiểm tra thông tin và lịch của nghệ sĩ và\n                        thông báo đến bạn qua email bạn đã đăng ký. Xin cảm ơn\n                        !\n                      ")]),_vm._v(" "),_c('div',{staticClass:"text-left"},[_c('span',{staticClass:"fw-600"},[_vm._v(" Mã booking: ")]),_vm._v(" "),_c('span',{staticClass:"font-weight-bold fs-15 black--text"},[_vm._v("\n                          "+_vm._s(_vm.randomCode))]),_vm._v(" "),_c('span',[_c('v-btn',{staticClass:"primary text-none ml-2",attrs:{"depressed":"","small":"","dense":""},on:{"click":function($event){return _vm.copy_text("copy-text")}}},[_vm._v("Copy\n                            "),_c('div',{staticClass:"d-none",attrs:{"id":"copy-text"}},[_vm._v("\n                              "+_vm._s(_vm.randomCode)+"\n                            ")])])],1)])],1),_vm._v(" "),_c('v-col',{staticClass:"px-0 border-text",attrs:{"cols":"12"}},[_c('div',{staticClass:"d-flex align-center"},[_c('v-img',{staticClass:"mr-4",attrs:{"height":"22","max-width":"22","src":"/icon/address.png"}}),_vm._v(" "),_c('div',{staticClass:"black--text fs-16"},[_vm._v("\n                          "+_vm._s(_vm.address)+"\n                        ")])],1)]),_vm._v(" "),_c('v-col',{staticClass:"px-0 border-text",attrs:{"cols":"12"}},[_c('div',{staticClass:"d-flex align-center"},[_c('v-img',{staticClass:"mr-4",attrs:{"height":"22","max-width":"22","src":"/VAB_booking/event.png"}}),_vm._v(" "),_c('div',{staticClass:"fs-16",staticStyle:{"color":"#088d08"}},[_vm._v("\n                          "+_vm._s(_vm.content)+"\n                        ")])],1)]),_vm._v(" "),_c('v-col',{staticClass:"px-0 border-text",attrs:{"cols":"12"}},[_c('v-row',[_c('v-col',{staticClass:"color-text fs-16 pb-0 px-0",attrs:{"cols":"4"}},[_vm._v("\n                          "+_vm._s(_vm.lang('$vuetify.pageBooking.history.artistName'))+":\n                        ")]),_vm._v(" "),_c('v-col',{staticClass:"pb-0 black--text",attrs:{"cols":"8"}},[_vm._v("\n                          "+_vm._s(_vm.data.artistName)+"\n                        ")]),_vm._v(" "),_c('v-col',{staticClass:"color-text fs-16 pb-0 px-0",attrs:{"cols":"4"}},[_vm._v("\n                          "+_vm._s(_vm.lang('$vuetify.pageBooking.dialogBook.startTime'))+":\n                        ")]),_vm._v(" "),(_vm.data.createTime)?_c('v-col',{staticClass:"pb-0 black--text",attrs:{"cols":"8"}},[_vm._v("\n                          "+_vm._s(_vm.data.createTime.slice(0, 10))+"\n                        ")]):_vm._e(),_vm._v(" "),_c('v-col',{staticClass:"color-text fs-16 pb-0 px-0",attrs:{"cols":"4"}},[_vm._v("\n                          "+_vm._s(_vm.lang('$vuetify.pageBooking.dialogBook.endTime'))+":\n                        ")]),_vm._v(" "),(_vm.data.fromTime)?_c('v-col',{staticClass:"pb-0 black--text",attrs:{"cols":"8"}},[_vm._v("\n                          "+_vm._s(_vm.data.fromTime.slice(0, 10))+"\n                        ")]):_vm._e(),_vm._v(" "),_c('v-col',{staticClass:"color-text fs-16 pb-0 px-0",attrs:{"cols":"4"}},[_vm._v("\n                          "+_vm._s(_vm.lang(
                              '$vuetify.pageBooking.dialogBook.expectedCost'
                            ))+":\n                        ")]),_vm._v(" "),_c('v-col',{staticClass:"pb-0 black--text",attrs:{"cols":"8"}},[_vm._v("\n                          "+_vm._s(_vm.$formatMoneyv2({ amount: _vm.expectFee }))+" VNĐ\n                        ")]),_vm._v(" "),_c('v-col',{staticClass:"color-text fs-16 pb-0 px-0",attrs:{"cols":"4"}},[_vm._v("\n                          Trạng thái:\n                        ")]),_vm._v(" "),_c('v-col',{staticClass:"pb-0 black--text",attrs:{"cols":"8"}},[_c('v-chip',[_vm._v("Đang chờ duyệt")])],1)],1)],1),_vm._v(" "),_c('v-col',{staticClass:"pa-0",attrs:{"cols":"12"}},[_c('div',{staticClass:"pb-2"},[_vm._v("Thông tin liên hệ")])]),_vm._v(" "),(_vm.nameCustomer)?_c('v-col',{staticClass:"pl-0 fs-16 pb-0",attrs:{"cols":"6"}},[_c('label',{staticClass:"black--text"},[_vm._v("\n                        Tên liên hệ (quản lý)\n                      ")]),_vm._v(" "),_c('v-text-field',{staticClass:"color-icon-time fs-15px pb-error-0 hoverText",attrs:{"readonly":"","persistent-hint":"","hide-details":"","outlined":"","dense":"","error-messages":_vm.nameCustomerErrors},on:{"input":function($event){_vm.nameCustomerErrors = []}},scopedSlots:_vm._u([{key:"prepend-inner",fn:function(){return [_c('v-img',{attrs:{"height":"23","width":"23","src":"/VAB_booking/user.png"}}),_vm._v(" "),_c('v-divider',{staticClass:"mt-0 ml-2",attrs:{"color":"#11B71F","inset":"","vertical":""}})]},proxy:true}],null,false,3500167227),model:{value:(_vm.nameCustomer),callback:function ($$v) {_vm.nameCustomer=$$v},expression:"nameCustomer"}})],1):_vm._e(),_vm._v(" "),(_vm.phoneNumber)?_c('v-col',{staticClass:"pl-0 fs-16 pb-0",attrs:{"cols":"6"}},[_c('label',{staticClass:"black--text"},[_vm._v("\n                        "+_vm._s(_vm.lang('$vuetify.pageBooking.dialogBook.phoneNumber'))+"\n                      ")]),_vm._v(" "),_c('a',{staticStyle:{"text-decoration":"none"},attrs:{"href":("tel:" + _vm.phoneNumber)}},[_c('v-text-field',{staticClass:"color-icon-time fs-15px pb-error-0 hoverText",attrs:{"persistent-hint":"","hide-details":"","readonly":"","outlined":"","dense":""},scopedSlots:_vm._u([{key:"prepend-inner",fn:function(){return [_c('v-img',{attrs:{"height":"23","width":"23","src":"/VAB_booking/phone.png"}}),_vm._v(" "),_c('v-divider',{staticClass:"mt-0 ml-2",attrs:{"color":"#11B71F","inset":"","vertical":""}})]},proxy:true}],null,false,3219336662),model:{value:(_vm.phoneNumber),callback:function ($$v) {_vm.phoneNumber=$$v},expression:"phoneNumber"}})],1)]):_vm._e(),_vm._v(" "),(_vm.gmail)?_c('v-col',{staticClass:"pl-0 fs-16 pb-0",attrs:{"cols":"6"}},[_c('label',{staticClass:"black--text"},[_vm._v("Email")]),_vm._v(" "),_c('a',{staticClass:"py-1 fs-14",staticStyle:{"text-decoration":"none"},attrs:{"href":("mailto:" + _vm.gmail)}},[_c('v-text-field',{staticClass:"color-icon-time fs-15px pb-error-0 hoverText",attrs:{"persistent-hint":"","hide-details":"","readonly":"","outlined":"","dense":"","error-messages":_vm.gmailErrors},on:{"input":function($event){_vm.gmailErrors = []}},scopedSlots:_vm._u([{key:"prepend-inner",fn:function(){return [_c('v-img',{attrs:{"height":"23","width":"23","src":"/VAB_booking/gmail.png"}}),_vm._v(" "),_c('v-divider',{staticClass:"mt-0 ml-2",attrs:{"color":"#11B71F","inset":"","vertical":""}})]},proxy:true}],null,false,2244500100),model:{value:(_vm.gmail),callback:function ($$v) {_vm.gmail=$$v},expression:"gmail"}})],1)]):_vm._e(),_vm._v(" "),(_vm.facebook)?_c('v-col',{staticClass:"pl-0 fs-16 pb-0",attrs:{"cols":"6"}},[_c('label',{staticClass:"black--text"},[_vm._v("Link Facebook")]),_vm._v(" "),_c('a',{staticStyle:{"text-decoration":"none"},attrs:{"href":_vm.facebook,"target":"_blank"}},[_c('v-text-field',{staticClass:"color-icon-time fs-15px pb-error-0 hoverText",attrs:{"persistent-hint":"","hide-details":"","readonly":"","outlined":"","dense":""},scopedSlots:_vm._u([{key:"prepend-inner",fn:function(){return [_c('v-img',{attrs:{"height":"23","width":"23","src":"/VAB_booking/facebook.png"}}),_vm._v(" "),_c('v-divider',{staticClass:"mt-0 ml-2",attrs:{"color":"#11B71F","inset":"","vertical":""}})]},proxy:true}],null,false,2601604258),model:{value:(_vm.facebook),callback:function ($$v) {_vm.facebook=$$v},expression:"facebook"}})],1)]):_vm._e(),_vm._v(" "),_c('v-col',{staticClass:"text-center pb-0 mt-5",attrs:{"cols":"12"}},[_c('v-btn',{staticClass:"custom-btn-normal mr-2",attrs:{"depressed":"","height":"40","width":"30%","color":"#1448C6"},on:{"click":_vm.comfirmBooking}},[_c('div',{staticClass:"font_size"},[_vm._v("Quản lý booking")])]),_vm._v(" "),_c('v-btn',{staticClass:"custom-btn-normal",attrs:{"depressed":"","height":"40","width":"30%","color":"#EF9C0E"},on:{"click":_vm.toggle}},[_c('div',{staticClass:"font_size"},[_vm._v("Thoát")])])],1)],1)],1)],1)],1):_vm._e()],1)])],1)],1),_vm._v(" "),_c('div',{staticStyle:{"position":"absolute","right":"8px","bottom":"40px"}},[_c('v-fab-transition',[_c('v-btn',{attrs:{"color":"#47bbff","top":"","right":"","fab":"","dark":"","absolute":""}},[_c('v-icon',{attrs:{"size":"35"}},[_vm._v("\n            "+_vm._s('mdi-facebook-messenger')+"\n          ")])],1)],1)],1)],1)],1)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/booking/detail.vue?vue&type=template&id=6aa13412&

// EXTERNAL MODULE: ./components/YesNoAlert.vue + 4 modules
var YesNoAlert = __webpack_require__(81);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/booking/detail.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var detailvue_type_script_lang_js_ = ({
  components: {
    YesNoAlert: YesNoAlert["default"]
  },
  props: {
    open: {
      type: Boolean
    },
    id: {
      type: Number,
      default: null
    },
    data: {
      type: Object,
      default: null
    }
  },

  data() {
    return {
      openDialogYesNo: false,
      address: '',
      randomCode: null,
      content: '',
      nameCustomer: '',
      nameCustomerErrors: [],
      gmail: '',
      gmailErrors: [],
      phoneNumber: '',
      phoneNumberErrors: [],
      facebook: '',
      messages: '',
      expectFee: ''
    };
  },

  watch: {
    open(value) {
      if (value) {
        this.detailArtistsBooking();
      }
    }

  },
  methods: {
    copy_text(elementId) {
      const aux = document.createElement('input'); // Assign it the value of the specified element

      const str = document.getElementById(elementId).innerHTML;
      aux.setAttribute('value', (str || '').trim()); // Append it to the body

      document.body.appendChild(aux); // Highlight its content

      aux.select(); // Copy the highlighted text

      document.execCommand('copy'); // Remove it from the body

      document.body.removeChild(aux);
      this.$store.dispatch('notification/set_notifications', {
        type: 'success',
        color: 'success',
        // text: this.lang('$vuetify.registrationAccountSuccessful'),
        text: 'Copy mã boking thành công',
        dark: true
      });
    },

    lang(value) {
      if (value) {
        return this.$vuetify.lang.t(value);
      }
    },

    comfirmBooking() {
      window.location.replace('/lich-su-booking-khach-hang');
      this.$emit('toggle'); // this.$store
      //   .dispatch('profile/comfirmBooking', {
      //     id: this.data.id,
      //     status: 0,
      //   })
      //   .then((res) => {
      //     if (!res.error) {
      //       this.$store.dispatch('notification/set_notifications', {
      //         type: 'success',
      //         color: 'success',
      //         text: 'Xác nhận booking thành công',
      //         dark: true,
      //       })
      //       this.toggle()
      //     }
      //   })
    },

    detailArtistsBooking() {
      this.nameCustomer = this.data.mnName;
      this.phoneNumber = this.data.phone;
      this.gmail = this.data.mnEmail;
      this.content = this.data.content;
      this.address = this.data.address;
      this.facebook = this.data.facebook;
      this.messages = this.data.mnFbmess;
      this.expectFee = this.data.expectFee;
      this.randomCode = this.data.randomCode;
      console.log(this.data, '123123');
    },

    toggle() {
      this.$emit('toggle');
    },

    cancel() {
      this.$store.dispatch('profile/deleteBooking', {
        id: this.data.id
      }).then(res => {
        if (!res.error) {
          this.$store.dispatch('notification/set_notifications', {
            type: 'success',
            color: 'success',
            text: 'Hủy booking thành công',
            dark: true
          });
          this.toggle();
        }
      });
    }

  }
});
// CONCATENATED MODULE: ./components/booking/detail.vue?vue&type=script&lang=js&
 /* harmony default export */ var booking_detailvue_type_script_lang_js_ = (detailvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(14);

// EXTERNAL MODULE: ./node_modules/vuetify-loader/lib/runtime/installComponents.js
var installComponents = __webpack_require__(16);
var installComponents_default = /*#__PURE__*/__webpack_require__.n(installComponents);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VBtn/VBtn.js
var VBtn = __webpack_require__(54);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/VCard.js
var VCard = __webpack_require__(100);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/index.js
var components_VCard = __webpack_require__(11);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VChip/VChip.js
var VChip = __webpack_require__(77);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VCol.js
var VCol = __webpack_require__(281);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VDialog/VDialog.js
var VDialog = __webpack_require__(284);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VDivider/VDivider.js
var VDivider = __webpack_require__(103);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/transitions/index.js + 2 modules
var transitions = __webpack_require__(29);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VIcon/VIcon.js
var VIcon = __webpack_require__(72);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VImg/VImg.js + 2 modules
var VImg = __webpack_require__(78);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VRow.js
var VRow = __webpack_require__(283);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VTextField/VTextField.js + 3 modules
var VTextField = __webpack_require__(25);

// CONCATENATED MODULE: ./components/booking/detail.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(367)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  booking_detailvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "2b95aa01"
  
)

/* harmony default export */ var detail = __webpack_exports__["default"] = (component.exports);

/* vuetify-loader */













installComponents_default()(component, {VBtn: VBtn["a" /* default */],VCard: VCard["a" /* default */],VCardText: components_VCard["b" /* VCardText */],VChip: VChip["a" /* default */],VCol: VCol["a" /* default */],VDialog: VDialog["a" /* default */],VDivider: VDivider["a" /* default */],VFabTransition: transitions["c" /* VFabTransition */],VIcon: VIcon["a" /* default */],VImg: VImg["a" /* default */],VRow: VRow["a" /* default */],VTextField: VTextField["a" /* default */]})


/***/ })

};;
//# sourceMappingURL=booking-detail.js.map