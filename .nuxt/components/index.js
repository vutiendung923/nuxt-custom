import { wrapFunctional } from './utils'

export { default as Dialog } from '../../components/Dialog.vue'
export { default as ProgressCircular } from '../../components/ProgressCircular.vue'
export { default as YesNoAlert } from '../../components/YesNoAlert.vue'
export { default as AlertErrorIcon } from '../../components/alert/errorIcon.vue'
export { default as Alert } from '../../components/alert/index.vue'
export { default as AlertSuccessIcon } from '../../components/alert/successIcon.vue'
export { default as ArtistsDialogSearch } from '../../components/artists/DialogSearch.vue'
export { default as BookingAddEmbeb } from '../../components/booking/addEmbeb.vue'
export { default as BookingAddProduct } from '../../components/booking/addProduct.vue'
export { default as BookingAddProductHot } from '../../components/booking/addProductHot.vue'
export { default as BookingAddStory } from '../../components/booking/addStory.vue'
export { default as BookingDetail } from '../../components/booking/detail.vue'
export { default as BookingDetailEmbeb } from '../../components/booking/detailEmbeb.vue'
export { default as BookingDetailStory } from '../../components/booking/detailStory.vue'
export { default as BookingDialogBooking } from '../../components/booking/dialogBooking.vue'
export { default as BookingViewEmbed } from '../../components/booking/viewEmbed.vue'
export { default as DepositDialogDeposit } from '../../components/deposit/dialogDeposit.vue'
export { default as DepositPaymentBankCard } from '../../components/deposit/paymentBankCard.vue'
export { default as DepositPaymentOnline } from '../../components/deposit/paymentOnline.vue'
export { default as HomePageNews } from '../../components/homePage/news.vue'
export { default as LoginDialogInfo } from '../../components/login/dialogInfo.vue'
export { default as LoginDialogSignUp } from '../../components/login/dialogSignUp.vue'
export { default as LoginDialogSuccessful } from '../../components/login/dialogSuccessful.vue'
export { default as LoginDialoglogin } from '../../components/login/dialoglogin.vue'
export { default as BookingBannerBookingDialogBanner } from '../../components/booking/bannerBooking/dialogBanner.vue'
export { default as BookingBannerBooking } from '../../components/booking/bannerBooking/index.vue'
export { default as BookingDateBooking } from '../../components/booking/dateBooking/index.vue'
export { default as BookingGoogleMap } from '../../components/booking/googleMap/GoogleMap.vue'
export { default as BookingHistoryComfirmBooking } from '../../components/booking/history/comfirmBooking.vue'
export { default as BookingHistory } from '../../components/booking/history/history.vue'
export { default as BookingHistorySearchBooking } from '../../components/booking/history/searchBooking.vue'
export { default as BookingProfileUpdateOther } from '../../components/booking/profile/UpdateOther.vue'
export { default as BookingProfileAddOther } from '../../components/booking/profile/addOther.vue'
export { default as BookingProfileProductOther } from '../../components/booking/profile/productOther.vue'
export { default as BookingProfile } from '../../components/booking/profile/profile.vue'
export { default as BookingScheduleComfirmBooking } from '../../components/booking/schedule/comfirmBooking.vue'
export { default as BookingSchedule } from '../../components/booking/schedule/index.vue'
export { default as BookingTutorialBooking } from '../../components/booking/tutorialBooking/index.vue'
export { default as DepositDepositAdd } from '../../components/deposit/deposit/add.vue'
export { default as DepositAdd } from '../../components/deposit/deposit/depositAdd.vue'
export { default as DepositUpdate } from '../../components/deposit/deposit/depositUpdate.vue'
export { default as DepositDepositListDeposit } from '../../components/deposit/deposit/listDeposit.vue'

export const LazyDialog = import('../../components/Dialog.vue' /* webpackChunkName: "components/dialog" */).then(c => wrapFunctional(c.default || c))
export const LazyProgressCircular = import('../../components/ProgressCircular.vue' /* webpackChunkName: "components/progress-circular" */).then(c => wrapFunctional(c.default || c))
export const LazyYesNoAlert = import('../../components/YesNoAlert.vue' /* webpackChunkName: "components/yes-no-alert" */).then(c => wrapFunctional(c.default || c))
export const LazyAlertErrorIcon = import('../../components/alert/errorIcon.vue' /* webpackChunkName: "components/alert-error-icon" */).then(c => wrapFunctional(c.default || c))
export const LazyAlert = import('../../components/alert/index.vue' /* webpackChunkName: "components/alert" */).then(c => wrapFunctional(c.default || c))
export const LazyAlertSuccessIcon = import('../../components/alert/successIcon.vue' /* webpackChunkName: "components/alert-success-icon" */).then(c => wrapFunctional(c.default || c))
export const LazyArtistsDialogSearch = import('../../components/artists/DialogSearch.vue' /* webpackChunkName: "components/artists-dialog-search" */).then(c => wrapFunctional(c.default || c))
export const LazyBookingAddEmbeb = import('../../components/booking/addEmbeb.vue' /* webpackChunkName: "components/booking-add-embeb" */).then(c => wrapFunctional(c.default || c))
export const LazyBookingAddProduct = import('../../components/booking/addProduct.vue' /* webpackChunkName: "components/booking-add-product" */).then(c => wrapFunctional(c.default || c))
export const LazyBookingAddProductHot = import('../../components/booking/addProductHot.vue' /* webpackChunkName: "components/booking-add-product-hot" */).then(c => wrapFunctional(c.default || c))
export const LazyBookingAddStory = import('../../components/booking/addStory.vue' /* webpackChunkName: "components/booking-add-story" */).then(c => wrapFunctional(c.default || c))
export const LazyBookingDetail = import('../../components/booking/detail.vue' /* webpackChunkName: "components/booking-detail" */).then(c => wrapFunctional(c.default || c))
export const LazyBookingDetailEmbeb = import('../../components/booking/detailEmbeb.vue' /* webpackChunkName: "components/booking-detail-embeb" */).then(c => wrapFunctional(c.default || c))
export const LazyBookingDetailStory = import('../../components/booking/detailStory.vue' /* webpackChunkName: "components/booking-detail-story" */).then(c => wrapFunctional(c.default || c))
export const LazyBookingDialogBooking = import('../../components/booking/dialogBooking.vue' /* webpackChunkName: "components/booking-dialog-booking" */).then(c => wrapFunctional(c.default || c))
export const LazyBookingViewEmbed = import('../../components/booking/viewEmbed.vue' /* webpackChunkName: "components/booking-view-embed" */).then(c => wrapFunctional(c.default || c))
export const LazyDepositDialogDeposit = import('../../components/deposit/dialogDeposit.vue' /* webpackChunkName: "components/deposit-dialog-deposit" */).then(c => wrapFunctional(c.default || c))
export const LazyDepositPaymentBankCard = import('../../components/deposit/paymentBankCard.vue' /* webpackChunkName: "components/deposit-payment-bank-card" */).then(c => wrapFunctional(c.default || c))
export const LazyDepositPaymentOnline = import('../../components/deposit/paymentOnline.vue' /* webpackChunkName: "components/deposit-payment-online" */).then(c => wrapFunctional(c.default || c))
export const LazyHomePageNews = import('../../components/homePage/news.vue' /* webpackChunkName: "components/home-page-news" */).then(c => wrapFunctional(c.default || c))
export const LazyLoginDialogInfo = import('../../components/login/dialogInfo.vue' /* webpackChunkName: "components/login-dialog-info" */).then(c => wrapFunctional(c.default || c))
export const LazyLoginDialogSignUp = import('../../components/login/dialogSignUp.vue' /* webpackChunkName: "components/login-dialog-sign-up" */).then(c => wrapFunctional(c.default || c))
export const LazyLoginDialogSuccessful = import('../../components/login/dialogSuccessful.vue' /* webpackChunkName: "components/login-dialog-successful" */).then(c => wrapFunctional(c.default || c))
export const LazyLoginDialoglogin = import('../../components/login/dialoglogin.vue' /* webpackChunkName: "components/login-dialoglogin" */).then(c => wrapFunctional(c.default || c))
export const LazyBookingBannerBookingDialogBanner = import('../../components/booking/bannerBooking/dialogBanner.vue' /* webpackChunkName: "components/booking-banner-booking-dialog-banner" */).then(c => wrapFunctional(c.default || c))
export const LazyBookingBannerBooking = import('../../components/booking/bannerBooking/index.vue' /* webpackChunkName: "components/booking-banner-booking" */).then(c => wrapFunctional(c.default || c))
export const LazyBookingDateBooking = import('../../components/booking/dateBooking/index.vue' /* webpackChunkName: "components/booking-date-booking" */).then(c => wrapFunctional(c.default || c))
export const LazyBookingGoogleMap = import('../../components/booking/googleMap/GoogleMap.vue' /* webpackChunkName: "components/booking-google-map" */).then(c => wrapFunctional(c.default || c))
export const LazyBookingHistoryComfirmBooking = import('../../components/booking/history/comfirmBooking.vue' /* webpackChunkName: "components/booking-history-comfirm-booking" */).then(c => wrapFunctional(c.default || c))
export const LazyBookingHistory = import('../../components/booking/history/history.vue' /* webpackChunkName: "components/booking-history" */).then(c => wrapFunctional(c.default || c))
export const LazyBookingHistorySearchBooking = import('../../components/booking/history/searchBooking.vue' /* webpackChunkName: "components/booking-history-search-booking" */).then(c => wrapFunctional(c.default || c))
export const LazyBookingProfileUpdateOther = import('../../components/booking/profile/UpdateOther.vue' /* webpackChunkName: "components/booking-profile-update-other" */).then(c => wrapFunctional(c.default || c))
export const LazyBookingProfileAddOther = import('../../components/booking/profile/addOther.vue' /* webpackChunkName: "components/booking-profile-add-other" */).then(c => wrapFunctional(c.default || c))
export const LazyBookingProfileProductOther = import('../../components/booking/profile/productOther.vue' /* webpackChunkName: "components/booking-profile-product-other" */).then(c => wrapFunctional(c.default || c))
export const LazyBookingProfile = import('../../components/booking/profile/profile.vue' /* webpackChunkName: "components/booking-profile" */).then(c => wrapFunctional(c.default || c))
export const LazyBookingScheduleComfirmBooking = import('../../components/booking/schedule/comfirmBooking.vue' /* webpackChunkName: "components/booking-schedule-comfirm-booking" */).then(c => wrapFunctional(c.default || c))
export const LazyBookingSchedule = import('../../components/booking/schedule/index.vue' /* webpackChunkName: "components/booking-schedule" */).then(c => wrapFunctional(c.default || c))
export const LazyBookingTutorialBooking = import('../../components/booking/tutorialBooking/index.vue' /* webpackChunkName: "components/booking-tutorial-booking" */).then(c => wrapFunctional(c.default || c))
export const LazyDepositDepositAdd = import('../../components/deposit/deposit/add.vue' /* webpackChunkName: "components/deposit-deposit-add" */).then(c => wrapFunctional(c.default || c))
export const LazyDepositAdd = import('../../components/deposit/deposit/depositAdd.vue' /* webpackChunkName: "components/deposit-add" */).then(c => wrapFunctional(c.default || c))
export const LazyDepositUpdate = import('../../components/deposit/deposit/depositUpdate.vue' /* webpackChunkName: "components/deposit-update" */).then(c => wrapFunctional(c.default || c))
export const LazyDepositDepositListDeposit = import('../../components/deposit/deposit/listDeposit.vue' /* webpackChunkName: "components/deposit-deposit-list-deposit" */).then(c => wrapFunctional(c.default || c))
