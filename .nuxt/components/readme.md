# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<Dialog>` | `<dialog>` (components/Dialog.vue)
- `<ProgressCircular>` | `<progress-circular>` (components/ProgressCircular.vue)
- `<YesNoAlert>` | `<yes-no-alert>` (components/YesNoAlert.vue)
- `<AlertErrorIcon>` | `<alert-error-icon>` (components/alert/errorIcon.vue)
- `<Alert>` | `<alert>` (components/alert/index.vue)
- `<AlertSuccessIcon>` | `<alert-success-icon>` (components/alert/successIcon.vue)
- `<ArtistsDialogSearch>` | `<artists-dialog-search>` (components/artists/DialogSearch.vue)
- `<BookingAddEmbeb>` | `<booking-add-embeb>` (components/booking/addEmbeb.vue)
- `<BookingAddProduct>` | `<booking-add-product>` (components/booking/addProduct.vue)
- `<BookingAddProductHot>` | `<booking-add-product-hot>` (components/booking/addProductHot.vue)
- `<BookingAddStory>` | `<booking-add-story>` (components/booking/addStory.vue)
- `<BookingDetail>` | `<booking-detail>` (components/booking/detail.vue)
- `<BookingDetailEmbeb>` | `<booking-detail-embeb>` (components/booking/detailEmbeb.vue)
- `<BookingDetailStory>` | `<booking-detail-story>` (components/booking/detailStory.vue)
- `<BookingDialogBooking>` | `<booking-dialog-booking>` (components/booking/dialogBooking.vue)
- `<BookingViewEmbed>` | `<booking-view-embed>` (components/booking/viewEmbed.vue)
- `<DepositDialogDeposit>` | `<deposit-dialog-deposit>` (components/deposit/dialogDeposit.vue)
- `<DepositPaymentBankCard>` | `<deposit-payment-bank-card>` (components/deposit/paymentBankCard.vue)
- `<DepositPaymentOnline>` | `<deposit-payment-online>` (components/deposit/paymentOnline.vue)
- `<HomePageNews>` | `<home-page-news>` (components/homePage/news.vue)
- `<LoginDialogInfo>` | `<login-dialog-info>` (components/login/dialogInfo.vue)
- `<LoginDialogSignUp>` | `<login-dialog-sign-up>` (components/login/dialogSignUp.vue)
- `<LoginDialogSuccessful>` | `<login-dialog-successful>` (components/login/dialogSuccessful.vue)
- `<LoginDialoglogin>` | `<login-dialoglogin>` (components/login/dialoglogin.vue)
- `<BookingBannerBookingDialogBanner>` | `<booking-banner-booking-dialog-banner>` (components/booking/bannerBooking/dialogBanner.vue)
- `<BookingBannerBooking>` | `<booking-banner-booking>` (components/booking/bannerBooking/index.vue)
- `<BookingDateBooking>` | `<booking-date-booking>` (components/booking/dateBooking/index.vue)
- `<BookingGoogleMap>` | `<booking-google-map>` (components/booking/googleMap/GoogleMap.vue)
- `<BookingHistoryComfirmBooking>` | `<booking-history-comfirm-booking>` (components/booking/history/comfirmBooking.vue)
- `<BookingHistory>` | `<booking-history>` (components/booking/history/history.vue)
- `<BookingHistorySearchBooking>` | `<booking-history-search-booking>` (components/booking/history/searchBooking.vue)
- `<BookingProfileUpdateOther>` | `<booking-profile-update-other>` (components/booking/profile/UpdateOther.vue)
- `<BookingProfileAddOther>` | `<booking-profile-add-other>` (components/booking/profile/addOther.vue)
- `<BookingProfileProductOther>` | `<booking-profile-product-other>` (components/booking/profile/productOther.vue)
- `<BookingProfile>` | `<booking-profile>` (components/booking/profile/profile.vue)
- `<BookingScheduleComfirmBooking>` | `<booking-schedule-comfirm-booking>` (components/booking/schedule/comfirmBooking.vue)
- `<BookingSchedule>` | `<booking-schedule>` (components/booking/schedule/index.vue)
- `<BookingTutorialBooking>` | `<booking-tutorial-booking>` (components/booking/tutorialBooking/index.vue)
- `<DepositDepositAdd>` | `<deposit-deposit-add>` (components/deposit/deposit/add.vue)
- `<DepositAdd>` | `<deposit-add>` (components/deposit/deposit/depositAdd.vue)
- `<DepositUpdate>` | `<deposit-update>` (components/deposit/deposit/depositUpdate.vue)
- `<DepositDepositListDeposit>` | `<deposit-deposit-list-deposit>` (components/deposit/deposit/listDeposit.vue)
