import Vue from 'vue'
import { wrapFunctional } from './utils'

const components = {
  Dialog: () => import('../../components/Dialog.vue' /* webpackChunkName: "components/dialog" */).then(c => wrapFunctional(c.default || c)),
  ProgressCircular: () => import('../../components/ProgressCircular.vue' /* webpackChunkName: "components/progress-circular" */).then(c => wrapFunctional(c.default || c)),
  YesNoAlert: () => import('../../components/YesNoAlert.vue' /* webpackChunkName: "components/yes-no-alert" */).then(c => wrapFunctional(c.default || c)),
  AlertErrorIcon: () => import('../../components/alert/errorIcon.vue' /* webpackChunkName: "components/alert-error-icon" */).then(c => wrapFunctional(c.default || c)),
  Alert: () => import('../../components/alert/index.vue' /* webpackChunkName: "components/alert" */).then(c => wrapFunctional(c.default || c)),
  AlertSuccessIcon: () => import('../../components/alert/successIcon.vue' /* webpackChunkName: "components/alert-success-icon" */).then(c => wrapFunctional(c.default || c)),
  ArtistsDialogSearch: () => import('../../components/artists/DialogSearch.vue' /* webpackChunkName: "components/artists-dialog-search" */).then(c => wrapFunctional(c.default || c)),
  BookingAddEmbeb: () => import('../../components/booking/addEmbeb.vue' /* webpackChunkName: "components/booking-add-embeb" */).then(c => wrapFunctional(c.default || c)),
  BookingAddProduct: () => import('../../components/booking/addProduct.vue' /* webpackChunkName: "components/booking-add-product" */).then(c => wrapFunctional(c.default || c)),
  BookingAddProductHot: () => import('../../components/booking/addProductHot.vue' /* webpackChunkName: "components/booking-add-product-hot" */).then(c => wrapFunctional(c.default || c)),
  BookingAddStory: () => import('../../components/booking/addStory.vue' /* webpackChunkName: "components/booking-add-story" */).then(c => wrapFunctional(c.default || c)),
  BookingDetail: () => import('../../components/booking/detail.vue' /* webpackChunkName: "components/booking-detail" */).then(c => wrapFunctional(c.default || c)),
  BookingDetailEmbeb: () => import('../../components/booking/detailEmbeb.vue' /* webpackChunkName: "components/booking-detail-embeb" */).then(c => wrapFunctional(c.default || c)),
  BookingDetailStory: () => import('../../components/booking/detailStory.vue' /* webpackChunkName: "components/booking-detail-story" */).then(c => wrapFunctional(c.default || c)),
  BookingDialogBooking: () => import('../../components/booking/dialogBooking.vue' /* webpackChunkName: "components/booking-dialog-booking" */).then(c => wrapFunctional(c.default || c)),
  BookingViewEmbed: () => import('../../components/booking/viewEmbed.vue' /* webpackChunkName: "components/booking-view-embed" */).then(c => wrapFunctional(c.default || c)),
  DepositDialogDeposit: () => import('../../components/deposit/dialogDeposit.vue' /* webpackChunkName: "components/deposit-dialog-deposit" */).then(c => wrapFunctional(c.default || c)),
  DepositPaymentBankCard: () => import('../../components/deposit/paymentBankCard.vue' /* webpackChunkName: "components/deposit-payment-bank-card" */).then(c => wrapFunctional(c.default || c)),
  DepositPaymentOnline: () => import('../../components/deposit/paymentOnline.vue' /* webpackChunkName: "components/deposit-payment-online" */).then(c => wrapFunctional(c.default || c)),
  HomePageNews: () => import('../../components/homePage/news.vue' /* webpackChunkName: "components/home-page-news" */).then(c => wrapFunctional(c.default || c)),
  LoginDialogInfo: () => import('../../components/login/dialogInfo.vue' /* webpackChunkName: "components/login-dialog-info" */).then(c => wrapFunctional(c.default || c)),
  LoginDialogSignUp: () => import('../../components/login/dialogSignUp.vue' /* webpackChunkName: "components/login-dialog-sign-up" */).then(c => wrapFunctional(c.default || c)),
  LoginDialogSuccessful: () => import('../../components/login/dialogSuccessful.vue' /* webpackChunkName: "components/login-dialog-successful" */).then(c => wrapFunctional(c.default || c)),
  LoginDialoglogin: () => import('../../components/login/dialoglogin.vue' /* webpackChunkName: "components/login-dialoglogin" */).then(c => wrapFunctional(c.default || c)),
  BookingBannerBookingDialogBanner: () => import('../../components/booking/bannerBooking/dialogBanner.vue' /* webpackChunkName: "components/booking-banner-booking-dialog-banner" */).then(c => wrapFunctional(c.default || c)),
  BookingBannerBooking: () => import('../../components/booking/bannerBooking/index.vue' /* webpackChunkName: "components/booking-banner-booking" */).then(c => wrapFunctional(c.default || c)),
  BookingDateBooking: () => import('../../components/booking/dateBooking/index.vue' /* webpackChunkName: "components/booking-date-booking" */).then(c => wrapFunctional(c.default || c)),
  BookingGoogleMap: () => import('../../components/booking/googleMap/GoogleMap.vue' /* webpackChunkName: "components/booking-google-map" */).then(c => wrapFunctional(c.default || c)),
  BookingHistoryComfirmBooking: () => import('../../components/booking/history/comfirmBooking.vue' /* webpackChunkName: "components/booking-history-comfirm-booking" */).then(c => wrapFunctional(c.default || c)),
  BookingHistory: () => import('../../components/booking/history/history.vue' /* webpackChunkName: "components/booking-history" */).then(c => wrapFunctional(c.default || c)),
  BookingHistorySearchBooking: () => import('../../components/booking/history/searchBooking.vue' /* webpackChunkName: "components/booking-history-search-booking" */).then(c => wrapFunctional(c.default || c)),
  BookingProfileUpdateOther: () => import('../../components/booking/profile/UpdateOther.vue' /* webpackChunkName: "components/booking-profile-update-other" */).then(c => wrapFunctional(c.default || c)),
  BookingProfileAddOther: () => import('../../components/booking/profile/addOther.vue' /* webpackChunkName: "components/booking-profile-add-other" */).then(c => wrapFunctional(c.default || c)),
  BookingProfileProductOther: () => import('../../components/booking/profile/productOther.vue' /* webpackChunkName: "components/booking-profile-product-other" */).then(c => wrapFunctional(c.default || c)),
  BookingProfile: () => import('../../components/booking/profile/profile.vue' /* webpackChunkName: "components/booking-profile" */).then(c => wrapFunctional(c.default || c)),
  BookingScheduleComfirmBooking: () => import('../../components/booking/schedule/comfirmBooking.vue' /* webpackChunkName: "components/booking-schedule-comfirm-booking" */).then(c => wrapFunctional(c.default || c)),
  BookingSchedule: () => import('../../components/booking/schedule/index.vue' /* webpackChunkName: "components/booking-schedule" */).then(c => wrapFunctional(c.default || c)),
  BookingTutorialBooking: () => import('../../components/booking/tutorialBooking/index.vue' /* webpackChunkName: "components/booking-tutorial-booking" */).then(c => wrapFunctional(c.default || c)),
  DepositDepositAdd: () => import('../../components/deposit/deposit/add.vue' /* webpackChunkName: "components/deposit-deposit-add" */).then(c => wrapFunctional(c.default || c)),
  DepositAdd: () => import('../../components/deposit/deposit/depositAdd.vue' /* webpackChunkName: "components/deposit-add" */).then(c => wrapFunctional(c.default || c)),
  DepositUpdate: () => import('../../components/deposit/deposit/depositUpdate.vue' /* webpackChunkName: "components/deposit-update" */).then(c => wrapFunctional(c.default || c)),
  DepositDepositListDeposit: () => import('../../components/deposit/deposit/listDeposit.vue' /* webpackChunkName: "components/deposit-deposit-list-deposit" */).then(c => wrapFunctional(c.default || c))
}

for (const name in components) {
  Vue.component(name, components[name])
  Vue.component('Lazy' + name, components[name])
}
